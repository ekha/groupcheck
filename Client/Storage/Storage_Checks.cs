﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using GroupCheckUtility.DataStorage.Entities;
using GroupCheckUtility.DataStorage.DbEntities;
using System.Threading;

namespace GroupCheckUtility.DataStorage
{
	public partial class Storage
	{
		[Table("Checks")]
		public class ChecksStorage : BaseStorageController
		{
			internal ChecksStorage(Storage storage) : base(storage) { }

			public StorageCheck GetByID(Guid id)
			{
				var row = storage.GetEntityByID(table, id);
				if (row == null) return null;
				var check = DbCheck.Parse(row);
				return check;
			}
			public List<StorageCheck> GetByGroupCheckID(Guid id)
			{
				var result = new List<StorageCheck>();
				foreach (DataRow row in table.Select($"GroupCheckID='{id}'"))
				{
					result.Add(DbCheck.Parse(row));
				}
				return result;
			}
			public void Save(StorageCheck check, bool commit = true)
			{
				storage.SaveEntityByID(table, check.Id, check, (obj, row) => DbCheck.FillRow((StorageCheck)obj, row), commit);
			}
			public void Delete(Guid checkID, bool commit = true)
			{
				storage.DeleteEntityByID(table, checkID, commit);
			}
		}
	}
}
