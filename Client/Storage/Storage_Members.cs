﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using GroupCheckUtility.DataStorage.Entities;
using GroupCheckUtility.DataStorage.DbEntities;
using System.Threading;

namespace GroupCheckUtility.DataStorage
{
	public partial class Storage
	{
		[Table("Members")]
		public class MembersStorage : BaseStorageController
		{
			internal MembersStorage(Storage storage) : base(storage) { }

			public StorageMember GetByID(Guid id)
			{
				var row = storage.GetEntityByID(table, id);
				return (row == null) ? null : DbMember.Parse(row);
			}
			public List<StorageMember> GetByGroupCheckID(Guid id)
			{
				var result = new List<StorageMember>();
				foreach (DataRow row in table.Select($"GroupCheckID='{id}'"))
				{
					result.Add(DbMember.Parse(row));
				}
				return result;
			}
			public void Save(StorageMember member, bool commit = true)
			{
				storage.SaveEntityByID(table, member.Id, member, (obj, row) => DbMember.FillRow((StorageMember)obj, row), commit);
			}
			public void Delete(Guid memberID, bool commit = true)
			{
				storage.DeleteEntityByID(table, memberID, commit);
			}
		}
	}
}
