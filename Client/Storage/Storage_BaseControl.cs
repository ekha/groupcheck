﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using GroupCheckUtility.DataStorage.Entities;
using GroupCheckUtility.DataStorage.DbEntities;
using System.Threading;

namespace GroupCheckUtility.DataStorage
{
	public partial class Storage
	{
		public abstract class BaseStorageController
		{
			protected Storage storage;
			protected DataTable table;
			protected BaseStorageController(Storage storage)
			{
				var tableName = this.GetType().GetCustomAttributes(true).Where(_ => _ is TableAttribute).Select(_ => (TableAttribute)_).Single().Name;
				this.storage = storage;
				this.table = this.storage.db.Tables[tableName];
			}
		}
	}
}
