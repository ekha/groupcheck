﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using GroupCheckUtility.DataStorage.Entities;
using GroupCheckUtility.DataStorage.DbEntities;
using System.Threading;

namespace GroupCheckUtility.DataStorage
{
	public partial class Storage
	{
		[Table("Users")]
		public class UsersStorage : BaseStorageController
		{
			internal UsersStorage(Storage storage) : base(storage) { }

			public StorageUser GetByID(Guid id)
			{
				var row = storage.GetEntityByID(table, id);
				return (row == null) ? null : DbUser.Parse(row);
			}
			public StorageUser GetByLogin(string Login)
			{
				var rows = table.Select($"Login='{Login}'");
				return (rows.Length == 0) ? null : DbUser.Parse(rows[0]);
			}
			public List<StorageUser> GetList()
			{
				var result = new List<StorageUser>();
				foreach (DataRow row in table.Select())
				{
					result.Add(DbUser.Parse(row));
				}
				return result;
			}
			public void Save(StorageUser user, bool commit = true)
			{
				storage.SaveEntityByID(table, user.Id, user, (obj, row) => DbUser.FillRow((StorageUser)obj, row), commit);
			}
			public void Delete(Guid userID, bool commit = true)
			{
				storage.DeleteEntityByID(table, userID, commit);
			}
		}
	}
}
