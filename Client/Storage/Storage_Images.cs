﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using GroupCheckUtility.DataStorage.Entities;
using GroupCheckUtility.DataStorage.DbEntities;
using System.Threading;

namespace GroupCheckUtility.DataStorage
{
	public partial class Storage
	{
		[Table("Images")]
		public class ImagesStorage : BaseStorageController
		{
			private const string PATH = ".\\images";
			internal ImagesStorage(Storage storage) : base(storage) { }
			private string GetFilePath(Guid ImageID)
			{
				return $"{PATH}\\{ImageID}";
			}

			public StorageImage GetByID(Guid id)
			{
				if (!System.IO.File.Exists(GetFilePath(id)))
					return null;

				var row = storage.GetEntityByID(table, id);
				if (row == null) return null;
				var image = DbImage.Parse(row);
				image.Data = System.IO.File.ReadAllBytes(GetFilePath(id));
				return image;
			}
			public void Save(StorageImage image, bool commit = true)
			{
				try
				{
					storage.BeginTransaction();
					storage.SaveEntityByID(table, image.Id, image, (obj, row) => DbImage.FillRow((StorageImage)obj, row), commit);
					System.IO.File.WriteAllBytes(GetFilePath(image.Id), image.Data);
					storage.CommitTransaction();
				}
				catch
				{
					storage.RollbackTransaction();
				}
			}
			public void Delete(Guid imageID, bool commit = true)
			{
				try
				{
					storage.BeginTransaction();
					storage.DeleteEntityByID(table, imageID, commit);
					if (System.IO.File.Exists(GetFilePath(imageID)))
						System.IO.File.Delete(GetFilePath(imageID));
					storage.CommitTransaction();
				}
				catch
				{
					storage.RollbackTransaction();
				}
			}
		}
	}
}
