﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using GroupCheckUtility.DataStorage.Entities;
using GroupCheckUtility.DataStorage.DbEntities;
using System.Threading;

namespace GroupCheckUtility.DataStorage
{
	public partial class Storage
	{
		[Table("Settings")]
		public class SettingsStorage : BaseStorageController
		{
			internal SettingsStorage(Storage storage) : base(storage) { }

			public T Get<T>(string Key) where T : struct
			{
				var rows = table.Select($"Key='{Key}'");
				return (rows.Length == 0) ? default(T) : rows[0].Value<T>("Value");
			}
			public void Set(string Key, object Value)
			{
				storage.BeginTransaction();
				try
				{
					var rows = table.Select($"Key='{Key}'");

					if (rows.Length == 0)
					{
						var row = table.NewRow();
						row["Key"] = Key;
						row["Value"] = Value;
						table.Rows.Add(row);
					}
					else
					{
						var row = rows[0];
						row["Key"] = Key;
						row["Value"] = Value;
					}
					storage.CommitTransaction();
				}
				catch
				{
					storage.RollbackTransaction();
					throw;
				}
			}
			public void Delete(string Key)
			{
				storage.BeginTransaction();
				try
				{
					var rows = table.Select($"Key='{Key}'");
					if (rows.Length > 0)
					{
						table.Rows.Remove(rows[0]);
					}
					storage.CommitTransaction();
				}
				catch
				{
					storage.RollbackTransaction();
					throw;
				}
			}
		}
	}
}
