﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace GroupCheckUtility.DataStorage.Entities
{
	public abstract class SyncEntity
	{
		public bool PendingSync { get; set; }
	}
}
