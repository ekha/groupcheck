﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace GroupCheckUtility.DataStorage.Entities
{
	public class StorageGroupCheck : SyncEntity
	{
		public Guid Id { get; set; }
		public string Description { get; set; }
		public Guid? AvatarId { get; set; }
		public DateTime Created { get; set; }
		public Guid CreatorId { get; set; }
		public DateTime Edited { get; set; }
		public Guid EditorId { get; set; }
	}
}
