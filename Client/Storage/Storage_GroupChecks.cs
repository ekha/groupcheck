﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using GroupCheckUtility.DataStorage.Entities;
using GroupCheckUtility.DataStorage.DbEntities;
using System.Threading;

namespace GroupCheckUtility.DataStorage
{
	public partial class Storage
	{
		[Table("GroupChecks")]
		public class GroupChecksStorage : BaseStorageController
		{
			internal GroupChecksStorage(Storage storage) : base(storage) { }

			public StorageGroupCheck GetByID(Guid id)
			{
				var row = storage.GetEntityByID(table, id);
				return (row == null) ? null : DbGroupCheck.Parse(row);
			}
			public List<StorageGroupCheck> GetList()
			{
				var result = new List<StorageGroupCheck>();
				foreach (DataRow row in table.Select())
				{
					result.Add(DbGroupCheck.Parse(row));
				}
				return result;
			}
			public void Save(StorageGroupCheck groupCheck, bool commit = true)
			{
				storage.SaveEntityByID(table, groupCheck.Id, groupCheck, (obj, row) => DbGroupCheck.FillRow((StorageGroupCheck)obj, row), commit);
			}
			public void Save(StorageGroupCheck groupCheck, List<StorageMember> members)
			{
				storage.BeginTransaction();
				try
				{
					var membersList = storage.Members.GetByGroupCheckID(groupCheck.Id).Select(x => x.Id).ToList();

					storage.GroupChecks.Save(groupCheck, false);
					foreach (var member in members)
					{
						storage.Members.Save(member, false);
						membersList.Remove(member.Id);
					}
					foreach (var member in membersList)
					{
						storage.Members.Delete(member, false);
					}

					storage.CommitTransaction();
				}
				catch
				{
					storage.RollbackTransaction();
					throw;
				}
			}
			public void Delete(Guid groupCheckID, bool commit = true)
			{
				storage.DeleteEntityByID(table, groupCheckID, commit);
			}
		}
	}
}
