﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using GroupCheckUtility.DataStorage.DbEntities;
using System.Threading;

namespace GroupCheckUtility.DataStorage
{
	static class DataSetExtensions
	{
		public static T Value<T>(this DataRow row, string ColumnName)
		{
			return (row[ColumnName] == null || row[ColumnName] == DBNull.Value) ? default(T) : (T)row[ColumnName];
		}
	}
	class TableAttribute : Attribute
	{
		public string Name { get; set; }
		public TableAttribute(string Name)
		{
			this.Name = Name;
		}
	}
}
