﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using GroupCheckUtility.DataStorage.DbEntities;
using System.Threading;

namespace GroupCheckUtility.DataStorage
{
	public partial class Storage
	{
		Guid userID;
		string filename;
		DataSet db, backup;
		static object tranLocker = new object();

		public ChecksStorage Checks { get; }
		public GroupChecksStorage GroupChecks { get; }
		public MembersStorage Members { get; }
		public SettingsStorage Settings { get; }
		public UsersStorage Users { get; }
		public ImagesStorage Images { get; }

		public Storage(Guid UserID)
		{
			userID = UserID;
			filename = userID + ".xml";
			db = new DataSet();
			if (System.IO.File.Exists(filename))
			{
				db.ReadXml(filename);
			}
			UpdateDb();

			Checks = new ChecksStorage(this);
			GroupChecks = new GroupChecksStorage(this);
			Members = new MembersStorage(this);
			Settings = new SettingsStorage(this);
			Users = new UsersStorage(this);
			Images = new ImagesStorage(this);
		}

		void UpdateDb()
		{
			var templateDb = CreateOriginalDb();
			if (db == null) db = new DataSet();
			if (db.DataSetName != templateDb.DataSetName) db.DataSetName = templateDb.DataSetName;

			var tablesToBeRemoved = new List<string>();
			foreach (DataTable table in db.Tables)
			{
				tablesToBeRemoved.Add(table.TableName);
			}

			foreach (DataTable templateTable in templateDb.Tables)
			{
				if (!db.Tables.Contains(templateTable.TableName)) db.Tables.Add(templateTable.TableName);

				var columnsToBeRemoved = new List<string>();
				foreach (DataColumn col in db.Tables[templateTable.TableName].Columns)
				{
					columnsToBeRemoved.Add(col.ColumnName);
				}

				foreach (DataColumn templateCol in templateTable.Columns)
				{
					if (!db.Tables[templateTable.TableName].Columns.Contains(templateCol.ColumnName))
						db.Tables[templateTable.TableName].Columns.Add(templateCol.ColumnName, templateCol.DataType);

					if (db.Tables[templateTable.TableName].Columns[templateCol.ColumnName].DataType != templateCol.DataType)
						db.Tables[templateTable.TableName].Columns[templateCol.ColumnName].DataType = templateCol.DataType;

					if (db.Tables[templateTable.TableName].Columns[templateCol.ColumnName].AllowDBNull != templateCol.AllowDBNull)
						db.Tables[templateTable.TableName].Columns[templateCol.ColumnName].AllowDBNull = templateCol.AllowDBNull;

					if (db.Tables[templateTable.TableName].Columns[templateCol.ColumnName].Unique != templateCol.Unique)
						db.Tables[templateTable.TableName].Columns[templateCol.ColumnName].Unique = templateCol.Unique;

					columnsToBeRemoved.Remove(templateCol.ColumnName);
				}

				foreach (var colName in columnsToBeRemoved)
				{
					db.Tables[templateTable.TableName].Columns.Remove(colName);
				}

				tablesToBeRemoved.Remove(templateTable.TableName);
			}

			foreach (var tableName in tablesToBeRemoved)
			{
				db.Tables.Remove(tableName);
			}
		}
		static DataSet CreateOriginalDb()
		{
			var ds = new DataSet("GroupCheck");
			ds.Tables.Add(DbUser.CreateDataTable());
			ds.Tables.Add(DbGroupCheck.CreateDataTable());
			ds.Tables.Add(DbMember.CreateDataTable());
			ds.Tables.Add(DbCheck.CreateDataTable());
			ds.Tables.Add(DbSettings.CreateDataTable());
			ds.Tables.Add(DbImage.CreateDataTable());
			return ds;
		}
		void Save()
		{
			db.WriteXml(filename, XmlWriteMode.WriteSchema);
		}
		void BeginTransaction()
		{
			Monitor.Enter(tranLocker);
			backup = db.Copy();
		}
		void CommitTransaction()
		{
			Save();
			Monitor.Exit(tranLocker);
		}
		void RollbackTransaction()
		{
			db = backup.Copy();
			Monitor.Exit(tranLocker);
		}
		DataRow GetEntityByID(DataTable table, Guid id)
		{
			var rows = table.Select($"ID='{id}'");
			if (rows.Length == 0)
			{
				return null;
			}
			else
			{
				return rows[0];
			}
		}
		void SaveEntityByID(DataTable table, Guid id, object obj, Action<object, DataRow> FillRow, bool commit = true)
		{
			if (commit) BeginTransaction();
			try
			{
				var rows = table.Select($"ID='{id}'");

				if (rows.Length == 0)
				{
					var row = table.NewRow();
					FillRow(obj, row);
					table.Rows.Add(row);
				}

				else
				{
					var row = rows[0];
					FillRow(obj, row);
				}

				if (commit) CommitTransaction();
			}
			catch
			{
				if (commit) RollbackTransaction();
				throw;
			}
		}
		void DeleteEntityByID(DataTable table, Guid id, bool commit = true)
		{
			if (commit) BeginTransaction();
			try
			{
				var rows = table.Select($"ID='{id}'");

				if (rows.Length > 0)
				{
					table.Rows.Remove(rows[0]);
				}

				if (commit) CommitTransaction();
			}
			catch
			{
				if (commit) RollbackTransaction();
				throw;
			}
		}
	}
}
