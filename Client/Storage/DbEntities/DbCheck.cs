﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using GroupCheckUtility.DataStorage.Entities;

namespace GroupCheckUtility.DataStorage.DbEntities
{
	internal static class DbCheck
	{
		internal static DataTable CreateDataTable()
		{
			var table = new DataTable("Checks");

			var id = new DataColumn("Id", typeof(Guid)) { AllowDBNull = false, Unique = true };
			var groupCheckId = new DataColumn("GroupCheckId", typeof(Guid));
			var dateAndTime = new DataColumn("DateAndTime", typeof(DateTime));
			var description = new DataColumn("Description", typeof(string));
			var creditors = new DataColumn("Creditors", typeof(string));
			var debitors = new DataColumn("Debitors", typeof(string));
			var created = new DataColumn("Created", typeof(DateTime));
			var creatorId = new DataColumn("CreatorId", typeof(Guid));
			var edited = new DataColumn("Edited", typeof(DateTime));
			var editorId = new DataColumn("EditorId", typeof(Guid));
			var pendingSync = new DataColumn("PendingSync", typeof(bool));

			table.Columns.Add(id);
			table.Columns.Add(groupCheckId);
			table.Columns.Add(dateAndTime);
			table.Columns.Add(description);
			table.Columns.Add(creditors);
			table.Columns.Add(debitors);
			table.Columns.Add(created);
			table.Columns.Add(creatorId);
			table.Columns.Add(edited);
			table.Columns.Add(editorId);
			table.Columns.Add(pendingSync);
			table.PrimaryKey = new DataColumn[] { id };

			return table;
		}
		internal static StorageCheck Parse(DataRow row)
		{
			return new StorageCheck()
			{
				Id = row.Value<Guid>("Id"),
				GroupCheckId = row.Value<Guid>("GroupCheckId"),
				DateAndTime = row.Value<DateTime>("DateAndTime"),
				Description = row.Value<string>("Description"),
				Creditors = Deserialize(row.Value<string>("Creditors")),
				Debitors = Deserialize(row.Value<string>("Debitors")),
				Created = row.Value<DateTime>("Created"),
				CreatorId = row.Value<Guid>("CreatorId"),
				Edited = row.Value<DateTime>("Edited"),
				EditorId = row.Value<Guid>("EditorId"),
				PendingSync = row.Value<bool>("PendingSync")
			};
		}
		internal static void FillRow(StorageCheck check, DataRow row)
		{
			row["ID"] = check.Id;
			row["GroupCheckId"] = check.GroupCheckId;
			row["DateAndTime"] = check.DateAndTime;
			row["Description"] = check.Description;
			row["Creditors"] = Serialize(check.Creditors);
			row["Debitors"] = Serialize(check.Debitors);
			row["Created"] = check.Created;
			row["CreatorId"] = check.CreatorId;
			row["Edited"] = check.Edited;
			row["EditorId"] = check.EditorId;
			row["PendingSync"] = check.PendingSync;
		}

		static string Serialize(List<StorageCheck.Participant> participants)
		{
			if (participants == null) return null;
			return string.Join(",", participants.Select(_ => $"{_.MemberId}={_.Summ}"));
		}
		static List<StorageCheck.Participant> Deserialize(string participants)
		{
			if (participants == null) return null;
			var result = new List<StorageCheck.Participant>();
			foreach (var s in participants.Split(','))
			{
				var t = s.Split('=');
				var id = Guid.Parse(t[0]);
				var summ = int.Parse(t[1]);
				result.Add(new StorageCheck.Participant() { MemberId = id, Summ = summ });
			}
			return result;
		}
	}
}
