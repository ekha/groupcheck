﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using GroupCheckUtility.DataStorage.Entities;

namespace GroupCheckUtility.DataStorage.DbEntities
{
	internal static class DbUser
	{
		internal static DataTable CreateDataTable()
		{
			var table = new DataTable("Users");

			var id = new DataColumn("Id", typeof(Guid)) { AllowDBNull = false, Unique = true };
			var name = new DataColumn("Name", typeof(string));
			var localName = new DataColumn("LocalName", typeof(string));
			var avatarId = new DataColumn("AvatarId", typeof(Guid)) { AllowDBNull = true };
			var registered = new DataColumn("Registered", typeof(DateTime));
			var edited = new DataColumn("Edited", typeof(DateTime));
			var pendingSync = new DataColumn("PendingSync", typeof(bool));

			table.Columns.Add(id);
			table.Columns.Add(name);
			table.Columns.Add(localName);
			table.Columns.Add(avatarId);
			table.Columns.Add(registered);
			table.Columns.Add(edited);
			table.Columns.Add(pendingSync);
			table.PrimaryKey = new DataColumn[] { id };

			return table;
		}
		internal static StorageUser Parse(DataRow row)
		{
			return new StorageUser()
			{
				Id = row.Value<Guid>("Id"),
				Name = row.Value<string>("Name"),
				LocalName = row.Value<string>("LocalName"),
				AvatarId = row.Value<Guid?>("AvatarId"),
				Registered = row.Value<DateTime>("Registered"),
				Edited = row.Value<DateTime>("Edited"),
				PendingSync = row.Value<bool>("PendingSync")
			};
		}
		internal static void FillRow(StorageUser user, DataRow row)
		{
			row["Id"] = user.Id;
			row["Name"] = user.Name;
			row["LocalName"] = user.LocalName;
			row["AvatarId"] = (user.AvatarId == null) ? (object)DBNull.Value : user.AvatarId.Value;
			row["Registered"] = user.Registered;
			row["Edited"] = user.Edited;
			row["PendingSync"] = user.PendingSync;
		}
	}
}
