﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using GroupCheckUtility.DataStorage.Entities;

namespace GroupCheckUtility.DataStorage.DbEntities
{
	internal static class DbGroupCheck
	{
		internal static DataTable CreateDataTable()
		{
			var table = new DataTable("GroupChecks");

			var id = new DataColumn("ID", typeof(Guid)) { AllowDBNull = false, Unique = true };
			var description = new DataColumn("Description", typeof(string));
			var baseCurrency = new DataColumn("BaseCurrency", typeof(string));
			var avatarID = new DataColumn("AvatarID", typeof(Guid)) { AllowDBNull = true };
			var created = new DataColumn("Created", typeof(DateTime));
			var creatorID = new DataColumn("CreatorID", typeof(Guid));
			var edited = new DataColumn("Edited", typeof(DateTime));
			var editorID = new DataColumn("EditorID", typeof(Guid));
			var pendingSync = new DataColumn("PendingSync", typeof(bool));

			table.Columns.Add(id);
			table.Columns.Add(description);
			table.Columns.Add(baseCurrency);
			table.Columns.Add(avatarID);
			table.Columns.Add(created);
			table.Columns.Add(creatorID);
			table.Columns.Add(edited);
			table.Columns.Add(editorID);
			table.Columns.Add(pendingSync);
			table.PrimaryKey = new DataColumn[] { id };

			return table;
		}
		internal static StorageGroupCheck Parse(DataRow row)
		{
			return new StorageGroupCheck()
			{
				Id = row.Value<Guid>("Id"),
				Description = row.Value<string>("Description"),
				AvatarId = row.Value<Guid?>("AvatarId"),
				Created = row.Value<DateTime>("Created"),
				CreatorId = row.Value<Guid>("CreatorId"),
				Edited = row.Value<DateTime>("Edited"),
				EditorId = row.Value<Guid>("EditorId"),
				PendingSync = row.Value<bool>("PendingSync")
			};
		}
		internal static void FillRow(StorageGroupCheck groupCheck, DataRow row)
		{
			row["Id"] = groupCheck.Id;
			row["Description"] = groupCheck.Description;
			row["AvatarId"] = (groupCheck.AvatarId == null) ? (object)DBNull.Value : groupCheck.AvatarId.Value;
			row["Created"] = groupCheck.Created;
			row["CreatorId"] = groupCheck.CreatorId;
			row["Edited"] = groupCheck.Edited;
			row["EditorId"] = groupCheck.EditorId;
			row["PendingSync"] = groupCheck.PendingSync;
		}
	}
}
