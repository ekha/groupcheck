﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using GroupCheckUtility.DataStorage.Entities;

namespace GroupCheckUtility.DataStorage.DbEntities
{
	internal static class DbSettings
	{
		internal static DataTable CreateDataTable()
		{
			var table = new DataTable("Settings");
			var key = new DataColumn("Key", typeof(string)) { AllowDBNull = false, Unique = true };
			var value = new DataColumn("Value", typeof(string));
			table.Columns.Add(key);
			table.Columns.Add(value);
			table.PrimaryKey = new DataColumn[] { key };
			return table;
		}
	}
}
