﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using GroupCheckUtility.DataStorage.Entities;

namespace GroupCheckUtility.DataStorage.DbEntities
{
	internal static class DbImage
	{
		internal static DataTable CreateDataTable()
		{
			var table = new DataTable("Images");

			var id = new DataColumn("Id", typeof(Guid)) { AllowDBNull = false, Unique = true };
			var filename = new DataColumn("Filename", typeof(string));
			//var data = new DataColumn("Data", typeof(byte[]));
			var created = new DataColumn("Created", typeof(DateTime));
			var creatorID = new DataColumn("CreatorId", typeof(Guid));
			var pendingSync = new DataColumn("PendingSync", typeof(bool));

			table.Columns.Add(id);
			table.Columns.Add(filename);
			//table.Columns.Add(data);
			table.Columns.Add(created);
			table.Columns.Add(creatorID);
			table.Columns.Add(pendingSync);
			table.PrimaryKey = new DataColumn[] { id };

			return table;
		}
		internal static StorageImage Parse(DataRow row)
		{
			return new StorageImage()
			{
				Id = row.Value<Guid>("Id"),
				Filename = row.Value<string>("Filename"),
				//Data = row.Value<byte[]>("Data"),
				Created = row.Value<DateTime>("Created"),
				CreatorId = row.Value<Guid>("CreatorId"),
				PendingSync = row.Value<bool>("PendingSync")
			};
		}
		internal static void FillRow(StorageImage image, DataRow row)
		{
			row["Id"] = image.Id;
			row["Filename"] = image.Filename;
			//row["Data"] = image.Data;
			row["Created"] = image.Created;
			row["CreatorId"] = image.CreatorId;
			row["PendingSync"] = image.PendingSync;
		}
	}
}
