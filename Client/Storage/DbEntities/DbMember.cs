﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using GroupCheckUtility.DataStorage.Entities;

namespace GroupCheckUtility.DataStorage.DbEntities
{
	internal static class DbMember
	{
		internal static DataTable CreateDataTable()
		{
			var table = new DataTable("Members");

			var id = new DataColumn("Id", typeof(Guid)) { AllowDBNull = false, Unique = true };
			var groupCheckId = new DataColumn("GroupCheckId", typeof(Guid));
			var userID = new DataColumn("UserID", typeof(Guid)) { AllowDBNull = true };
			var name = new DataColumn("Name", typeof(string));
			var isActive = new DataColumn("IsActive", typeof(bool));
			var created = new DataColumn("Created", typeof(DateTime));
			var creatorId = new DataColumn("CreatorId", typeof(Guid));
			var edited = new DataColumn("Edited", typeof(DateTime));
			var editorId = new DataColumn("EditorId", typeof(Guid));
			var pendingSync = new DataColumn("PendingSync", typeof(bool));

			table.Columns.Add(id);
			table.Columns.Add(groupCheckId);
			table.Columns.Add(userID);
			table.Columns.Add(name);
			table.Columns.Add(isActive);
			table.Columns.Add(created);
			table.Columns.Add(creatorId);
			table.Columns.Add(edited);
			table.Columns.Add(editorId);
			table.Columns.Add(pendingSync);
			table.PrimaryKey = new DataColumn[] { id };

			return table;
		}
		internal static StorageMember Parse(DataRow row)
		{
			return new StorageMember()
			{
				Id = row.Value<Guid>("Id"),
				GroupCheckId = row.Value<Guid>("GroupCheckId"),
				UserId = row.Value<Guid?>("UserId"),
				Name = row.Value<string>("Name"),
				Created = row.Value<DateTime>("Created"),
				CreatorId = row.Value<Guid>("CreatorId"),
				Edited = row.Value<DateTime>("Edited"),
				EditorId = row.Value<Guid>("EditorId"),
				PendingSync = row.Value<bool>("PendingSync")
			};
		}
		internal static void FillRow(StorageMember member, DataRow row)
		{
			row["Id"] = member.Id;
			row["GroupCheckId"] = member.GroupCheckId;
			row["UserId"] = (member.UserId == null) ? (object)DBNull.Value : member.UserId.Value;
			row["Name"] = member.Name;
			row["Created"] = member.Created;
			row["CreatorId"] = member.CreatorId;
			row["Edited"] = member.Edited;
			row["EditorId"] = member.EditorId;
			row["PendingSync"] = member.PendingSync;
		}
	}
}
