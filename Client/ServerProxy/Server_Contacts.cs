﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using GroupCheckUtility.ServerProxy.Entities;
using System.Net;

namespace GroupCheckUtility.ServerProxy
{
	public partial class Server
	{
		public List<ServerUser> FindUsers(string filter)
		{
			ServerResponse response;
			List<ServerUser> result;
			if (Get<List<ServerUser>>("Account/Find?Filter=" + filter, out response, out result))
			{
				return result;
			}
			else
			{
				throw new Exception(response.ErrorMassage);
			}
		}
	}
}
