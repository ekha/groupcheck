﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using GroupCheckUtility.ServerProxy.Entities;
using System.Net;

namespace GroupCheckUtility.ServerProxy
{
	public partial class Server
	{
		public void Login(string Email, string Password)
		{
			login = Email;
			password = Password;
			Authorize();
		}
		private void LoginThread(string Email, string Password)
		{
			login = Email;
			password = Password;
			Authorize();
		}
		public void Logout()
		{
			try
			{
				var response = MakeRequest("POST", $"{URL}/api/Account/Logout", "application/x-www-form-urlencoded", null, null);
			}
			catch (Exception ex)
			{
			}
		}
		public ServerUser Register(string Email, string Password)
		{
			var data = new { Email = Email, Password = Password };
			var body = data.Serialize();
			var response = MakeRequest("POST", $"{URL}/{API}/Account/Register", "application/json", null, body.ToString());

			if (response.HttpStatus == WebExceptionStatus.Success && response.StatusCode == HttpStatusCode.OK)
			{
				var user = response.Content.Deserialize<ServerUser>();
				return user;
			}
			else
			{
				throw new Exception(response.ErrorMassage);
			}
		}
		public ServerUser GetProfile()
		{
			ServerResponse response;
			ServerUser user;

			if (Get<ServerUser>($"Account/Profile", out response, out user))
			{
				return user;
			}
			else
			{
				throw new Exception($"{response.HttpStatus}:{response.StatusCode}:{response.Content}");
			}
		}
	}
}
