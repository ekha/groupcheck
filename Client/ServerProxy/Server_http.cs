﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using GroupCheckUtility.ServerProxy.Entities;
using System.Net;

namespace GroupCheckUtility.ServerProxy
{
	public partial class Server
	{
		private bool Get<T>(string uri, out ServerResponse response, out T result)
		{
			response = HttpGet(uri);
			if (response.HttpStatus == WebExceptionStatus.Success && response.StatusCode == HttpStatusCode.OK && response.Content.TryDeserialize<T>(out result))
			{
				return true;
			}
			else
			{
				result = default(T);
				return false;
			}
		}
		private bool Post<T>(string uri, object data, out ServerResponse response, out T result)
		{
			response = HttpPost(uri, data);
			if (response.HttpStatus == WebExceptionStatus.Success && response.StatusCode == HttpStatusCode.OK && response.Content.TryDeserialize<T>(out result))
			{
				return true;
			}
			else
			{
				result = default(T);
				return false;
			}
		}
		private ServerResponse HttpGet(string uri)
		{
			return SafeRequest("GET", uri, "application/json", null, null);
		}
		private ServerResponse HttpPost(string uri, object body)
		{
			return SafeRequest("POST", uri, "application/json", null, body.Serialize());
		}
		private ServerResponse SafeRequest(string method, string uri, string contentType, Dictionary<string, string> headers, string body)
		{
			if (securityToken == null)
			{
				Authorize();
			}

			if (headers == null) headers = new Dictionary<string, string>();
			if (!headers.ContainsKey("Authorization")) headers.Add("Authorization", $"Bearer {securityToken}");
			var response = MakeRequest(method, $"{URL}/{API}/{uri}", contentType, headers, body);

			if (response.StatusCode == HttpStatusCode.Unauthorized)
			{
				Authorize();
				response = MakeRequest(method, $"{URL}/{uri}", contentType, headers, body);
			}

			return response;
		}
		private void Authorize()
		{
			var body = $"grant_type=password&username={login}&password={password}";
			var response = MakeRequest("POST", $"{URL}/Token", "application/x-www-form-urlencoded", null, body.ToString());

			if (response.HttpStatus == WebExceptionStatus.Success && response.StatusCode == HttpStatusCode.OK)
			{
				var token = response.Content.Deserialize<GetTokenResponse>();
				securityToken = token.access_token;
			}
			else
			{
				throw new System.Security.Authentication.AuthenticationException(response.ErrorMassage);
			}
		}
		private static ServerResponse MakeRequest(string method, string uri, string contentType, Dictionary<string, string> headers, string body)
		{
			var result = new ServerResponse();

			var request = WebRequest.Create(uri);
			request.Method = method;
			request.ContentType = contentType;

			if (headers != null)
			{
				foreach (var header in headers)
				{
					request.Headers.Add(header.Key, header.Value);
				}
			}

			try
			{
				if (body != null)
				{
					var data = Encoding.UTF8.GetBytes(body);
					request.ContentLength = data.Length;
					using (var writer = request.GetRequestStream())
					{
						writer.Write(data, 0, data.Length);
						writer.Close();
					}
				}
				else if (request.Method == "POST")
				{
					request.ContentLength = 0;
				}

				using (var response = (HttpWebResponse)request.GetResponse())
				{
					result.HttpStatus = WebExceptionStatus.Success;
					result.StatusCode = response.StatusCode;
					result.Headers = new Dictionary<string, string>();
					if (response?.Headers != null)
					{
						for (int i = 0; i < response.Headers.Count; i++)
						{
							result.Headers.Add(response.Headers.Keys[i], response.Headers[i]);
						}
					}

					using (StreamReader stream = new StreamReader(response.GetResponseStream()))
					{
						result.Content = stream.ReadToEnd();
					}
				}
			}
			catch (WebException webEx)
			{
				result.HttpStatus = webEx.Status;

				if (webEx.Response != null)
				{
					using (var response = (HttpWebResponse)webEx.Response)
					{
						result.StatusCode = response.StatusCode;
						result.Headers = new Dictionary<string, string>();
						if (response?.Headers != null)
						{
							for (int i = 0; i < response.Headers.Count; i++)
							{
								result.Headers.Add(response.Headers.Keys[i], response.Headers[i]);
							}
						}

						try
						{
							var error = new ErrorResponse();
							var modelError = new ModelStateError();

							result.ErrorMassage = "Server error.";
							using (StreamReader stream = new StreamReader(response.GetResponseStream()))
							{
								result.Content = stream.ReadToEnd();
								if (!string.IsNullOrEmpty(result.Content))
								{
									if (result.Content.TryDeserialize<ErrorResponse>(out error) && !string.IsNullOrEmpty(error?.error_description))
										result.ErrorMassage = error.error_description;
									else if (result.Content.TryDeserialize<ModelStateError>(out modelError) && !string.IsNullOrEmpty(modelError?.Message))
										result.ErrorMassage = $"{modelError.Message}:\n{string.Join("\n", modelError.ModelState.First().Value.Select(_ => "\t- " + _))}";
								}
							}
						}
						catch { }
					}
				}
			}
			catch (Exception ex)
			{
				result.HttpStatus = WebExceptionStatus.UnknownError;
				result.StatusCode = HttpStatusCode.SeeOther;
				result.Headers = new Dictionary<string, string>();
				result.ErrorMassage = ex.Message;
			}

			return result;
		}
	}
}
