﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace GroupCheckUtility.ServerProxy.Entities
{
	public class ServerMember
	{
		public Guid Id { get; set; }
		public Guid GroupCheckId { get; set; }
		public Guid? UserId { get; set; }
		public string Name { get; set; }
		public DateTime Created { get; set; }
		public Guid CreatorId { get; set; }
		public DateTime Edited { get; set; }
		public Guid EditorId { get; set; }
	}
}
