﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace GroupCheckUtility.ServerProxy.Entities
{
	public class ServerUser
	{
		public Guid Id { get; set; }
		public string Name { get; set; }
		public Guid? AvatarId { get; set; }
		public DateTime Registered { get; set; }
		public DateTime Edited { get; set; }
	}
}
