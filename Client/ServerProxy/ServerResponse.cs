﻿using System;
using System.Collections.Generic;
using System.Net;

namespace GroupCheckUtility.ServerProxy.Entities
{
	public class ServerResponse
	{
		public WebExceptionStatus HttpStatus { get; set; }
		public HttpStatusCode StatusCode { get; set; }
		public string ErrorMassage { get; set; }
		public Dictionary<string, string> Headers { get; set; }
		public string Content { get; set; }
	}
	public class ErrorResponse
	{
		public string error { get; set; }
		public string error_description { get; set; }
	}
	public class ModelStateError
	{
		public string Message { get; set; }
		public Dictionary<string, List<string>> ModelState { get; set; }
	}
	public class GetTokenResponse
	{
		public string access_token { get; set; }
		public string token_type { get; set; }
		public string expires_in { get; set; }
		public string userName { get; set; }
	}
}
