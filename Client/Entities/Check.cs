﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace GroupCheckUtility.Entities
{
	public class Check
	{
		public Guid Id { get; set; }
		public Guid GroupCheckId { get; set; }
		public DateTime DateAndTime { get; set; }
		public string Description { get; set; }
		public List<Participant> Creditors { get; set; }
		public List<Participant> Debitors { get; set; }

		public DateTime Created { get; set; }
		public Guid CreatorId { get; set; }
		public DateTime Edited { get; set; }
		public Guid EditorId { get; set; }

		public class Participant
		{
			public Guid MemberId { get; set; }
			public int Summ { get; set; }
		}
	}
}
