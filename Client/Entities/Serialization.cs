﻿using System;

namespace GroupCheckUtility
{
	public static class Serializer
	{
		public static string Serialize(this object obj)
		{
			return Newtonsoft.Json.JsonConvert.SerializeObject(obj);
		}
		public static T Deserialize<T>(this string data)
		{
			return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(data);
		}
		public static bool TryDeserialize<T>(this string data, out T obj)
		{
			obj = default(T);

			try
			{
				obj = Newtonsoft.Json.JsonConvert.DeserializeObject<T>(data);
				return true;
			}
			catch
			{
				return false;
			}
		}
	}
}
