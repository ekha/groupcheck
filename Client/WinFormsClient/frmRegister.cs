﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using GroupCheckUtility.Core;
namespace GroupCheckClient
{
	public partial class frmRegister : GroupCheckClient.frmBase
	{
		public frmRegister()
		{
			InitializeComponent();

			Controller.ProfileController.OnRegisterFailed += ProfileController_OnRegisterFailed;
			Controller.ProfileController.OnRegisterFinished += ProfileController_OnRegisterFinished;
		}

		private void ProfileController_OnRegisterFinished(Profile profile)
		{
			StopProgress();
			MessageBox.Show(profile.Id.ToString());
		}

		private void ProfileController_OnRegisterFailed(string message)
		{
			StopProgress();
			MessageBox.Show(message, "Register failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
		}

		private void btnRegister_Click(object sender, EventArgs e)
		{
			var profile = new Profile()
			{
				Email = txtEmail.Text.Trim(),
				Password = txtPassword.Text
			};
			StartProgress();
			System.Threading.Tasks.Task.Run(() =>
			{

				Controller.ProfileController.Register(profile);
			});
		}

		private void frmRegister_Load(object sender, EventArgs e)
		{
			titleLabel.Text = "Register";
		}

		private void linkLogin_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
		{
			var frm = new frmLogin();
			frm.ShowDialog();
			this.Close();
		}
	}
}
