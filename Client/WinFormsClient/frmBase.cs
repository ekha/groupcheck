﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using GroupCheckUtility.Core;

namespace GroupCheckClient
{
	public partial class frmBase : Form
	{

		public frmBase()
		{
			InitializeComponent();

			if (openedForms.Count == 0)
				this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			openedForms.Add(this);
		}

		private void mainButtonClose_Click(object sender, EventArgs e)
		{
			Application.Exit();
		}
		private void mainButtonHome_Click(object sender, EventArgs e)
		{
			this.WindowState = FormWindowState.Minimized;
		}
		private void mainButtonBack_Click(object sender, EventArgs e)
		{
			this.DialogResult = DialogResult.Cancel;
			this.Close();
		}
		private void topButtonMenu_MouseUp(object sender, MouseEventArgs e)
		{
			if (TopMenu.Visible) return;

			int x = this.Left + topButtonMenu.Left + topButtonMenu.Width;
			int y = this.Top + topButtonMenu.Top;
			TopMenu.Show(topButtonMenu, -TopMenu.Width + topButtonMenu.Width, 0);
			TopMenu.Hide();
			TopMenu.Show(topButtonMenu, -TopMenu.Width + topButtonMenu.Width, 0);
		}


		bool isDragged = false;
		int x, y;
		static List<frmBase> openedForms = new List<frmBase>();
		private void panelMainControl_MouseDown(object sender, MouseEventArgs e)
		{
			isDragged = true;
			x = e.X;
			y = e.Y;

			foreach (var f in openedForms)
			{
				if (f != this)
				{
					f.Visible = false;
				}
			}
		}
		private void panelMainControl_MouseMove(object sender, MouseEventArgs e)
		{
			if (!isDragged) return;
			this.Left += e.X - x;
			this.Top += e.Y - y;
		}
		private void panelMainControl_MouseUp(object sender, MouseEventArgs e)
		{
			if (!isDragged) return;

			isDragged = false;

			foreach (var f in openedForms)
			{
				if (f != this)
				{
					f.Left = this.Left;
					f.Top = this.Top;
					f.Visible = true;
				}
			}
		}
		private void frmBase_FormClosed(object sender, FormClosedEventArgs e)
		{
			openedForms.Remove(this);
		}

		private void mainProgressTimer_Tick(object sender, EventArgs e)
		{
			mainProgress.Value = (mainProgress.Value + 10) % mainProgress.Maximum;
		}

		protected void StartProgress()
		{
			mainProgress.Value = 0;
			mainProgress.Visible = true;
			mainProgressTimer.Start();
		}
		protected void StopProgress()
		{
			mainProgressTimer.Stop();
			mainProgress.Visible = false;
		}
	}
}
