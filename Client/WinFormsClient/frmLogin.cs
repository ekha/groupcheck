﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using GroupCheckUtility.Core;

namespace GroupCheckClient
{
	public partial class frmLogin : GroupCheckClient.frmBase
	{
		public frmLogin()
		{
			InitializeComponent();

			Controller.ProfileController.OnLoginFailed += ProfileController_OnLoginFailed;
			Controller.ProfileController.OnProfileReceivedFailed += ProfileController_OnProfileReceivedFailed;
			Controller.ProfileController.OnProfileReceived += ProfileController_OnProfileReceived;
		}

		private void ProfileController_OnProfileReceived(Profile profile)
		{
			StopProgress();
			MessageBox.Show(profile.Id.ToString());
		}

		private void ProfileController_OnLoginFailed(string message)
		{
			StopProgress();
			MessageBox.Show(message, "Login failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
		}
		private void ProfileController_OnProfileReceivedFailed(string message)
		{
			StopProgress();
			MessageBox.Show(message, "Profile receive failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
		}

		private void btnLogin_Click(object sender, EventArgs e)
		{
			StartProgress();
			System.Threading.Tasks.Task.Run(() =>
			{
				Controller.ProfileController.GetProfileFromServer(txtEmail.Text.Trim(), txtPassword.Text);
			});
		}

		private void frmLogin_Load(object sender, EventArgs e)
		{
			titleLabel.Text = "Login";
		}

		private void linkRegister_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
		{
			var frm = new frmRegister();
			frm.ShowDialog();
			this.Close();
		}
	}
}
