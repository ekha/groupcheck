﻿namespace GroupCheckClient
{
	partial class frmRegister
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnRegister = new System.Windows.Forms.Button();
			this.txtPassword = new System.Windows.Forms.TextBox();
			this.lblPassword = new System.Windows.Forms.Label();
			this.txtEmail = new System.Windows.Forms.TextBox();
			this.lblEmail = new System.Windows.Forms.Label();
			this.linkLogin = new System.Windows.Forms.LinkLabel();
			this.panelHeader.SuspendLayout();
			this.SuspendLayout();
			// 
			// btnRegister
			// 
			this.btnRegister.Location = new System.Drawing.Point(163, 259);
			this.btnRegister.Name = "btnRegister";
			this.btnRegister.Size = new System.Drawing.Size(87, 27);
			this.btnRegister.TabIndex = 11;
			this.btnRegister.Text = "Register";
			this.btnRegister.UseVisualStyleBackColor = true;
			this.btnRegister.Click += new System.EventHandler(this.btnRegister_Click);
			// 
			// txtPassword
			// 
			this.txtPassword.Location = new System.Drawing.Point(50, 230);
			this.txtPassword.Name = "txtPassword";
			this.txtPassword.Size = new System.Drawing.Size(200, 23);
			this.txtPassword.TabIndex = 10;
			// 
			// lblPassword
			// 
			this.lblPassword.AutoSize = true;
			this.lblPassword.Location = new System.Drawing.Point(47, 212);
			this.lblPassword.Name = "lblPassword";
			this.lblPassword.Size = new System.Drawing.Size(64, 15);
			this.lblPassword.TabIndex = 9;
			this.lblPassword.Text = "Password:";
			// 
			// txtEmail
			// 
			this.txtEmail.Location = new System.Drawing.Point(50, 186);
			this.txtEmail.Name = "txtEmail";
			this.txtEmail.Size = new System.Drawing.Size(200, 23);
			this.txtEmail.TabIndex = 8;
			// 
			// lblEmail
			// 
			this.lblEmail.AutoSize = true;
			this.lblEmail.Location = new System.Drawing.Point(47, 168);
			this.lblEmail.Name = "lblEmail";
			this.lblEmail.Size = new System.Drawing.Size(41, 15);
			this.lblEmail.TabIndex = 7;
			this.lblEmail.Text = "Email:";
			// 
			// linkLogin
			// 
			this.linkLogin.AutoSize = true;
			this.linkLogin.Location = new System.Drawing.Point(47, 271);
			this.linkLogin.Name = "linkLogin";
			this.linkLogin.Size = new System.Drawing.Size(35, 15);
			this.linkLogin.TabIndex = 13;
			this.linkLogin.TabStop = true;
			this.linkLogin.Text = "login";
			this.linkLogin.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLogin_LinkClicked);
			// 
			// frmRegister
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
			this.ClientSize = new System.Drawing.Size(300, 520);
			this.Controls.Add(this.linkLogin);
			this.Controls.Add(this.btnRegister);
			this.Controls.Add(this.txtPassword);
			this.Controls.Add(this.lblPassword);
			this.Controls.Add(this.txtEmail);
			this.Controls.Add(this.lblEmail);
			this.Name = "frmRegister";
			this.Text = "Register";
			this.Load += new System.EventHandler(this.frmRegister_Load);
			this.Controls.SetChildIndex(this.panelHeader, 0);
			this.Controls.SetChildIndex(this.lblEmail, 0);
			this.Controls.SetChildIndex(this.txtEmail, 0);
			this.Controls.SetChildIndex(this.lblPassword, 0);
			this.Controls.SetChildIndex(this.txtPassword, 0);
			this.Controls.SetChildIndex(this.btnRegister, 0);
			this.Controls.SetChildIndex(this.linkLogin, 0);
			this.panelHeader.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button btnRegister;
		private System.Windows.Forms.TextBox txtPassword;
		private System.Windows.Forms.Label lblPassword;
		private System.Windows.Forms.TextBox txtEmail;
		private System.Windows.Forms.Label lblEmail;
		private System.Windows.Forms.LinkLabel linkLogin;
	}
}
