﻿namespace GroupCheckClient
{
	partial class frmBase
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.panelMainControl = new System.Windows.Forms.Panel();
			this.mainButtonClose = new System.Windows.Forms.Button();
			this.mainButtonHome = new System.Windows.Forms.Button();
			this.mainButtonBack = new System.Windows.Forms.Button();
			this.panelHeader = new System.Windows.Forms.Panel();
			this.titleLabel = new System.Windows.Forms.Label();
			this.topButtonMenu = new System.Windows.Forms.Label();
			this.TopMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.mainProgress = new System.Windows.Forms.ProgressBar();
			this.mainProgressTimer = new System.Windows.Forms.Timer(this.components);
			this.panelMainControl.SuspendLayout();
			this.panelHeader.SuspendLayout();
			this.SuspendLayout();
			// 
			// panelMainControl
			// 
			this.panelMainControl.BackColor = System.Drawing.SystemColors.WindowFrame;
			this.panelMainControl.Controls.Add(this.mainButtonClose);
			this.panelMainControl.Controls.Add(this.mainButtonHome);
			this.panelMainControl.Controls.Add(this.mainButtonBack);
			this.panelMainControl.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panelMainControl.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.panelMainControl.Location = new System.Drawing.Point(0, 460);
			this.panelMainControl.Name = "panelMainControl";
			this.panelMainControl.Size = new System.Drawing.Size(300, 60);
			this.panelMainControl.TabIndex = 0;
			this.panelMainControl.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panelMainControl_MouseDown);
			this.panelMainControl.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panelMainControl_MouseMove);
			this.panelMainControl.MouseUp += new System.Windows.Forms.MouseEventHandler(this.panelMainControl_MouseUp);
			// 
			// mainButtonClose
			// 
			this.mainButtonClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.mainButtonClose.ForeColor = System.Drawing.Color.White;
			this.mainButtonClose.Location = new System.Drawing.Point(210, 10);
			this.mainButtonClose.Name = "mainButtonClose";
			this.mainButtonClose.Size = new System.Drawing.Size(40, 40);
			this.mainButtonClose.TabIndex = 1;
			this.mainButtonClose.TabStop = false;
			this.mainButtonClose.Text = "■";
			this.mainButtonClose.UseVisualStyleBackColor = true;
			this.mainButtonClose.Click += new System.EventHandler(this.mainButtonClose_Click);
			// 
			// mainButtonHome
			// 
			this.mainButtonHome.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.mainButtonHome.ForeColor = System.Drawing.Color.White;
			this.mainButtonHome.Location = new System.Drawing.Point(129, 10);
			this.mainButtonHome.Name = "mainButtonHome";
			this.mainButtonHome.Size = new System.Drawing.Size(40, 40);
			this.mainButtonHome.TabIndex = 1;
			this.mainButtonHome.TabStop = false;
			this.mainButtonHome.Text = "▲";
			this.mainButtonHome.UseVisualStyleBackColor = true;
			this.mainButtonHome.Click += new System.EventHandler(this.mainButtonHome_Click);
			// 
			// mainButtonBack
			// 
			this.mainButtonBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.mainButtonBack.ForeColor = System.Drawing.Color.White;
			this.mainButtonBack.Location = new System.Drawing.Point(50, 10);
			this.mainButtonBack.Name = "mainButtonBack";
			this.mainButtonBack.Size = new System.Drawing.Size(40, 40);
			this.mainButtonBack.TabIndex = 1;
			this.mainButtonBack.TabStop = false;
			this.mainButtonBack.Text = "◀";
			this.mainButtonBack.UseVisualStyleBackColor = true;
			this.mainButtonBack.Click += new System.EventHandler(this.mainButtonBack_Click);
			// 
			// panelHeader
			// 
			this.panelHeader.BackColor = System.Drawing.SystemColors.InactiveCaption;
			this.panelHeader.Controls.Add(this.titleLabel);
			this.panelHeader.Controls.Add(this.topButtonMenu);
			this.panelHeader.Dock = System.Windows.Forms.DockStyle.Top;
			this.panelHeader.Location = new System.Drawing.Point(0, 0);
			this.panelHeader.Name = "panelHeader";
			this.panelHeader.Size = new System.Drawing.Size(300, 46);
			this.panelHeader.TabIndex = 0;
			// 
			// titleLabel
			// 
			this.titleLabel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.titleLabel.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.titleLabel.Location = new System.Drawing.Point(0, 0);
			this.titleLabel.Name = "titleLabel";
			this.titleLabel.Padding = new System.Windows.Forms.Padding(12, 0, 0, 0);
			this.titleLabel.Size = new System.Drawing.Size(253, 46);
			this.titleLabel.TabIndex = 1;
			this.titleLabel.Text = "title";
			this.titleLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// topButtonMenu
			// 
			this.topButtonMenu.Dock = System.Windows.Forms.DockStyle.Right;
			this.topButtonMenu.Location = new System.Drawing.Point(253, 0);
			this.topButtonMenu.Name = "topButtonMenu";
			this.topButtonMenu.Size = new System.Drawing.Size(47, 46);
			this.topButtonMenu.TabIndex = 0;
			this.topButtonMenu.Text = "...";
			this.topButtonMenu.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.topButtonMenu.MouseUp += new System.Windows.Forms.MouseEventHandler(this.topButtonMenu_MouseUp);
			// 
			// TopMenu
			// 
			this.TopMenu.Name = "Menu";
			this.TopMenu.ShowImageMargin = false;
			this.TopMenu.ShowItemToolTips = false;
			this.TopMenu.Size = new System.Drawing.Size(36, 4);
			// 
			// mainProgress
			// 
			this.mainProgress.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.mainProgress.Location = new System.Drawing.Point(0, 447);
			this.mainProgress.Name = "mainProgress";
			this.mainProgress.Size = new System.Drawing.Size(300, 13);
			this.mainProgress.Step = 1;
			this.mainProgress.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
			this.mainProgress.TabIndex = 1;
			this.mainProgress.Visible = false;
			// 
			// mainProgressTimer
			// 
			this.mainProgressTimer.Tick += new System.EventHandler(this.mainProgressTimer_Tick);
			// 
			// frmBase
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(300, 520);
			this.Controls.Add(this.mainProgress);
			this.Controls.Add(this.panelHeader);
			this.Controls.Add(this.panelMainControl);
			this.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.MaximizeBox = false;
			this.Name = "frmBase";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "frmBase";
			this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmBase_FormClosed);
			this.panelMainControl.ResumeLayout(false);
			this.panelHeader.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion
		private System.Windows.Forms.Button mainButtonBack;
		private System.Windows.Forms.Button mainButtonClose;
		private System.Windows.Forms.Button mainButtonHome;
		protected System.Windows.Forms.ContextMenuStrip TopMenu;
		protected System.Windows.Forms.Label topButtonMenu;
		protected System.Windows.Forms.Panel panelHeader;
		private System.Windows.Forms.Panel panelMainControl;
		protected System.Windows.Forms.Label titleLabel;
		private System.Windows.Forms.ProgressBar mainProgress;
		private System.Windows.Forms.Timer mainProgressTimer;
	}
}