﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using GroupCheckUtility.ServerProxy.Entities;
using GroupCheckUtility.DataStorage.Entities;

namespace GroupCheckUtility.Core.Entities
{
	public class GroupCheck
	{
		public Guid Id { get; set; }
		public string Description { get; set; }
		public Guid? AvatarId { get; set; }
		public List<Member> Members { get; set; }
		public List<Check> Checks { get; set; }
		public DateTime Created { get; set; }
		public Guid CreatorId { get; set; }
		public DateTime Edited { get; set; }
		public Guid EditorId { get; set; }
		public bool PendingSync { get; set; }
	}
}
