﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using GroupCheckUtility.ServerProxy.Entities;
using GroupCheckUtility.DataStorage.Entities;

namespace GroupCheckUtility.Core.Entities
{
	public class Image
	{
		public Guid Id { get; set; }
		public string Filename { get; set; }
		public byte[] Data { get; set; }
		public DateTime Created { get; set; }
		public Guid CreatorId { get; set; }
	}
}
