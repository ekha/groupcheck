﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using GroupCheckUtility.ServerProxy.Entities;
using GroupCheckUtility.DataStorage.Entities;

namespace GroupCheckUtility.Core.Entities
{
	public class Member
	{
		public Guid Id { get; set; }
		public User User { get; set; }
		public DateTime Edited { get; set; }
		public bool PendingSync { get; set; }
	}
}
