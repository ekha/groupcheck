﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using GroupCheckUtility.ServerProxy.Entities;
using GroupCheckUtility.DataStorage.Entities;

namespace GroupCheckUtility.Core
{
	public class Profile
	{
		public Guid Id { get; set; }
		public string Email { get; set; }
		public string Password { get; set; }
		public string Name { get; set; }
		public Guid? AvatarId { get; set; }
		public DateTime Registered { get; set; }
		public DateTime Edited { get; set; }
		public bool PendingSync { get; set; }

		public Profile() { }
		public Profile(StorageUser user)
		{
			Id = user.Id;
			Name = user.Name;
			AvatarId = user.AvatarId;
			Registered = user.Registered;
			Edited = user.Edited;
		}
		public Profile(ServerUser user)
		{
			Id = user.Id;
			Name = user.Name;
			AvatarId = user.AvatarId;
			Registered = user.Registered;
			Edited = user.Edited;
		}
		public StorageUser StorageUser
		{
			get
			{
				return new StorageUser()
				{
					Id = this.Id,
					Name = this.Name,
					LocalName = this.Name,
					AvatarId = this.AvatarId,
					Registered = this.Registered,
					Edited = this.Edited
				};
			}
		}
		public ServerUser ServerUser
		{
			get
			{
				return new ServerUser()
				{
					Id = this.Id,
					Name = this.Name,
					AvatarId = this.AvatarId,
					Registered = this.Registered,
					Edited = this.Edited
				};
			}
		}
	}
}
