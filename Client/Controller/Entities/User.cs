﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using GroupCheckUtility.ServerProxy.Entities;
using GroupCheckUtility.DataStorage.Entities;

namespace GroupCheckUtility.Core.Entities
{
	public class User
	{
		public Guid Id { get; set; }
		public string Name { get; set; }
		public string LocalName { get; set; }
		public Guid? AvatarId { get; set; }
		public DateTime Registered { get; set; }
		public DateTime Edited { get; set; }
		public bool PendingSync { get; set; }

		public User() { }
		public User(StorageUser user)
		{
			Id = user.Id;
			Name = user.Name;
			LocalName = user.LocalName;
			AvatarId = user.AvatarId;
			Registered = user.Registered;
			Edited = user.Edited;
			PendingSync = user.PendingSync;
		}
		public User(ServerUser user)
		{
			Id = user.Id;
			Name = user.Name;
			LocalName = user.Name;
			AvatarId = user.AvatarId;
			Registered = user.Registered;
			Edited = user.Edited;
		}
		public StorageUser StorageUser
		{
			get
			{
				return new StorageUser()
				{
					Id = this.Id,
					Name = this.Name,
					LocalName = this.LocalName,
					AvatarId = this.AvatarId,
					Registered = this.Registered,
					Edited = this.Edited,
					PendingSync = this.PendingSync
				};
			}
		}
		public ServerUser ServerUser
		{
			get
			{
				return new ServerUser()
				{
					Id = this.Id,
					Name = this.Name,
					AvatarId = this.AvatarId,
					Registered = this.Registered,
					Edited = this.Edited
				};
			}
		}

		public static bool operator ==(User user1, User user2)
		{
			return
				user1.Id == user2.Id &&
				user1.Name == user2.Name &&
				user1.LocalName == user2.LocalName &&
				user1.AvatarId == user2.AvatarId;
		}
		public static bool operator !=(User user1, User user2)
		{
			return !(user1 == user2);
		}
	}
}
