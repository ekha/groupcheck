﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using GroupCheckUtility.ServerProxy.Entities;
using GroupCheckUtility.DataStorage.Entities;

namespace GroupCheckUtility.Core.Entities
{
	public class Check
	{
		public Guid Id { get; set; }
		public DateTime DateAndTime { get; set; }
		public string Description { get; set; }
		public List<Participant> Creditors { get; set; }
		public List<Participant> Debitors { get; set; }
		public DateTime Edited { get; set; }
		public bool PendingSync { get; set; }

		public class Participant
		{
			public Member Member { get; set; }
			public int Summ { get; set; }
		}
	}
}
