﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Threading;
using GroupCheckUtility.ServerProxy;
using GroupCheckUtility.ServerProxy.Entities;
using GroupCheckUtility.DataStorage;
using GroupCheckUtility.DataStorage.Entities;

namespace GroupCheckUtility.Core
{
	public partial class Controller
	{
		public class ProfileController : BaseController
		{
			internal ProfileController(Controller controller) : base(controller) { }

			public static Profile GetLastUsedProfile()
			{
				return Configuration.Get<Profile>("Profile");
			}
			public static void GetProfileFromServer(string email, string password)
			{
				var server = new Server();

				try
				{
					server.Login(email, password);
				}
				catch (Exception ex)
				{
					OnLoginFailed?.Invoke(ex.Message);
					return;
				}

				try
				{
					var user = server.GetProfile();
					var profile = new Profile(user) { Email = email, Password = password };
					OnProfileReceived?.Invoke(profile);
				}
				catch (Exception ex)
				{
					OnProfileReceivedFailed?.Invoke(ex.Message);
				}
			}
			public static void Register(Profile newProfile)
			{
				try
				{
					var server = new Server();
					var user = server.Register(newProfile.Email, newProfile.Password);
					var profile = new Profile(user) { Email = newProfile.Email, Password = newProfile.Password };
					OnRegisterFinished?.Invoke(profile);
				}
				catch (Exception ex)
				{
					OnRegisterFailed?.Invoke(ex.Message);
				}
			}
			public void Save(Profile profile)
			{
				profile.PendingSync = true;
				storage.Users.Save(profile.StorageUser);
				Configuration.Save("Profile", profile);
			}
			public void SetAvatar(StorageImage Avatar)
			{
				Avatar.CreatorId = profile.Id;
				Avatar.Created = Now();
				storage.Images.Save(Avatar);

				profile.AvatarId = Avatar.Id;
				profile.PendingSync = true;

				storage.Users.Save(profile.StorageUser);
				Configuration.Save("Profile", profile);
			}

			public static event ServerError OnRegisterFailed;
			public static event ServerError OnLoginFailed;
			public static event ServerError OnProfileReceivedFailed;
			public static event ProfileDelegate OnRegisterFinished;
			public static event ProfileDelegate OnProfileReceived;
		}
	}
}
