﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using GroupCheckUtility.DataStorage;
using GroupCheckUtility.DataStorage.Entities;
using GroupCheckUtility.ServerProxy;
using GroupCheckUtility.ServerProxy.Entities;

namespace GroupCheckUtility.Core
{
	public partial class Controller
	{
		public abstract class BaseController
		{
			protected Controller controller;
			protected Profile profile;
			protected Server server;
			protected Storage storage;
			protected BaseController(Controller controller)
			{
				this.controller = controller;
				this.profile = controller.profile;
				this.server = controller.server;
				this.storage = controller.storage;
			}
		}
	}
}
