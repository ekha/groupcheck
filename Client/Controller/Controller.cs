﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using GroupCheckUtility.Core.Entities;
using GroupCheckUtility.DataStorage;
using GroupCheckUtility.DataStorage.Entities;
using GroupCheckUtility.ServerProxy;
using GroupCheckUtility.ServerProxy.Entities;

namespace GroupCheckUtility.Core
{
	public partial class Controller
	{
		Profile profile;
		Storage storage;
		Server server;

		public ProfileController Profile { get; }
		public ContactsController Contacts { get; }
		public GroupChecksController GroupChecks { get; }
		public ChecksController Checks { get; }
		public ImagesController Images { get; }

		public Controller(Profile profile)
		{
			this.profile = profile;
			storage = new Storage(this.profile.Id);
			server = new Server();
			server.Login(this.profile.Email, this.profile.Password);

			Profile = new ProfileController(this);
			Contacts = new ContactsController(this);
			GroupChecks = new GroupChecksController(this);
			Checks = new ChecksController(this);
			Images = new ImagesController(this);

			Configuration.Save("Profile", this.profile);
		}
	}

	public delegate void ServerResponse();
	public delegate void ServerError(string message);
	public delegate void ProfileDelegate(Profile profile);
	public delegate void UserDelegate(User user);
}
