﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Threading;
using GroupCheckUtility.Core.Entities;
using GroupCheckUtility.DataStorage;
using GroupCheckUtility.DataStorage.Entities;
using GroupCheckUtility.ServerProxy;
using GroupCheckUtility.ServerProxy.Entities;

namespace GroupCheckUtility.Core
{
	public partial class Controller
	{
		public class ContactsController : BaseController
		{
			internal ContactsController(Controller controller) : base(controller) { }

			public List<User> Find(string filter)
			{
				var users = server.FindUsers(filter);
				var contacts = users.Select(user => new User(user)).ToList();
				return contacts;
			}
			public List<User> GetContacts()
			{
				return storage.Users.GetList().Where(_ => _.Id != controller.profile.Id).Select(user => new User(user)).ToList();
			}
			public void Save(User contact)
			{
				var dbContact = storage.Users.GetByID(contact.Id);
				if (contact == new User(dbContact)) return;

				contact.PendingSync = true;
				storage.Users.Save(contact.StorageUser);
				OnUserUpdated?.Invoke(contact);
			}

			public event UserUpdated OnUserUpdated;
		}

		public delegate void UserUpdated(User UserInfo);
	}
}
