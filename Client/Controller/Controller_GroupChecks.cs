﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using GroupCheckUtility.DataStorage.Entities;
using System.Threading;
using GroupCheckUtility.ServerProxy;
using GroupCheckUtility.DataStorage;

namespace GroupCheckUtility.Core
{
	public partial class Controller
	{
		public class GroupChecksController : BaseController
		{
			internal GroupChecksController(Controller controller) : base(controller) { }

			public List<StorageGroupCheck> GetList()
			{
				return storage.GroupChecks.GetList();
			}
			public StorageGroupCheck Create(StorageGroupCheck NewGroupCheck, List<StorageMember> Members)
			{
				var now = Now();
				NewGroupCheck.Created = now;
				NewGroupCheck.CreatorId = profile.Id;
				NewGroupCheck.Edited = now;
				NewGroupCheck.EditorId = profile.Id;

				foreach (var member in Members)
				{
					member.Created = now;
					member.CreatorId = profile.Id;
					member.Edited = now;
					member.EditorId = profile.Id;
				}
				storage.GroupChecks.Save(NewGroupCheck, Members);

				return NewGroupCheck;
			}
		}
	}
}
