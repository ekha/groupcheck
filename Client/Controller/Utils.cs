﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using GroupCheckUtility.DataStorage;
using GroupCheckUtility.DataStorage.Entities;
using GroupCheckUtility.ServerProxy;
using GroupCheckUtility.ServerProxy.Entities;
using System.Security.Cryptography;


namespace GroupCheckUtility.Core
{
	public partial class Controller
	{
		/// <summary>
		/// Gets the current universal time.
		/// </summary>
		/// <returns></returns>
		public static DateTime Now()
		{
			return DateTime.Now.ToUniversalTime();
		}
	}

    public class Rnd
    {
        private static RNGCryptoServiceProvider rnd = new RNGCryptoServiceProvider();
        private static Random rnds = new Random();

        public static int Int()
        {
            return rnds.Next();
            //var bytes = new byte[4];
            //rnd.GetBytes(bytes);
            //return BitConverter.ToInt32(bytes, 0);
        }
        public static T[] Shuffle<T>(T[] array)
        {
            return array.OrderBy(x => rnds.Next()).ToArray();
        }
    }

    class Check
    {
        static decimal[] GetDebits(decimal[] credits, int count)
        {
            return GetDebits(credits.Sum(), count);
        }
        static decimal[] GetDebits(decimal summ, int count)
        {
            var s = (int)(summ * 100);
            var p = s / count;
            var r = s - p * count;

            var result = new decimal[count];
            for (int i = 0; i < r; i++)
            {
                result[i] = (decimal)(p + 1) / 100m;
            }
            for (int i = r; i < count; i++)
            {
                result[i] = (decimal)p / 100m;
            }
            result = Rnd.Shuffle(result);
            return result;
        }
        static void Convert(decimal[] credits, out decimal[] convertedCredits, decimal[] debits, out decimal[] convertedDebits, decimal course)
        {
            convertedCredits = new decimal[credits.Length];
            convertedDebits = new decimal[debits.Length];

            for (int i = 0; i < credits.Length; i++)
            {
                convertedCredits[i] = (decimal)Math.Round(credits[i] * course * 100m) / 100m;
            }
            for (int i = 0; i < debits.Length; i++)
            {
                convertedDebits[i] = (decimal)Math.Round(debits[i] * course * 100m) / 100m;
            }

            while (convertedCredits.Sum() < convertedDebits.Sum())
            {
                var n = (int)((convertedDebits.Sum() - convertedCredits.Sum()) * 100);
                var pos = convertedDebits.OrderByDescending(x => x).Take(n).Select((s, i) => i);
                foreach (var i in pos)
                    convertedDebits[i] -= 0.01m;
            }
            while (convertedCredits.Sum() > convertedDebits.Sum())
            {
                var n = (int)((convertedCredits.Sum() - convertedDebits.Sum()) * 100);
                var pos = convertedDebits.OrderByDescending(x => x).Take(n).Select((s, i) => i);
                foreach (var i in pos)
                    convertedDebits[i] += 0.01m;
            }

            var credit = convertedCredits.Sum();
            var debit = convertedDebits.Sum();
        }
    }
}
