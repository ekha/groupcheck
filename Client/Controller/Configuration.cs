﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using GroupCheckUtility.DataStorage;
using GroupCheckUtility.DataStorage.Entities;
using GroupCheckUtility.ServerProxy;
using GroupCheckUtility.ServerProxy.Entities;
using System.Threading;
using Newtonsoft.Json;

namespace GroupCheckUtility.Core
{
	public static class Configuration
	{
		static object locker = new object();
		static Dictionary<string, string> config = new Dictionary<string, string>();

		public static T Get<T>(string Key)
		{
			lock (locker)
			{
				return (config.ContainsKey(Key) && config[Key] != null) ? JsonConvert.DeserializeObject<T>(config[Key]) : default(T);
			}
		}
		public static void Save(string Key, object Value)
		{
			lock (locker)
			{
				config[Key] = JsonConvert.SerializeObject(Value);
				Save();
			}
		}
		public static void Delete(string Key)
		{
			lock (locker)
			{
				if (config.ContainsKey(Key))
				{
					config.Remove(Key);
					Save();
				}
			}
		}

		static string filename = "config.json";
		static Configuration()
		{
			Load();
		}
		static void Load()
		{
			lock (locker)
			{
				if (System.IO.File.Exists(filename))
				{
					var data = System.IO.File.ReadAllText(filename);
					config = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
				}
				else
				{
					config = new Dictionary<string, string>();
				}
			}
		}
		static void Save()
		{
			lock (locker)
			{
				var data = JsonConvert.SerializeObject(config);
				System.IO.File.WriteAllText(filename, data);
			}
		}
	}
}
