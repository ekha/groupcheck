﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using GroupCheckUtility.DataStorage.Entities;
using System.Threading;
using GroupCheckUtility.ServerProxy;
using GroupCheckUtility.DataStorage;

namespace GroupCheckUtility.Core
{
	public partial class Controller
	{
		public class ImagesController : BaseController
		{
			internal ImagesController(Controller controller) : base(controller) { }

			public StorageImage GetImage(Guid ImageID)
			{
				return storage.Images.GetByID(ImageID);
			}
			public void Save(StorageImage Img)
			{
				storage.Images.Save(Img);
			}
		}
	}
}
