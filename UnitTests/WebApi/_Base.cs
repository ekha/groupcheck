﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using GroupCheck.WebApi;
using GroupCheck.UnitTests;

namespace GroupCheck.UnitTests.WebApi
{
	public class _Base
	{
		protected static ServerProxy.GroupCheckAdapter GetHost()
		{
			return new ServerProxy.GroupCheckAdapter("http://localhost/gc");
		}

		[SetUp]
		public void DerivedSetUp()
		{
			var storage = new GroupCheck.ServerStorage.MSSQLStorage(".", "sa", "Aw34eszxc", "groupcheck");
			storage.SysRestoreBackup("c:\\SQL\\_Backups\\empty.bak", "c:\\SQL\\");
		}

		[TearDown]
		public void DerivedTearDown()
		{
			//var storage = new GroupCheck.ServerStorage.MSSQLStorage(".", "sa", "Aw34eszxc", "groupcheck");
			//storage.SysRestoreBackup("c:\\SQL\\_Backups\\empty.bak", "c:\\SQL\\");
		}
	}
}
