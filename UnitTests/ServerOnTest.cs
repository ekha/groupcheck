﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using GroupCheck.Server;

namespace GroupCheck.UnitTests.Server
{
	public class ServerOnTest : IDisposable
	{
		public readonly IServer Server;
		private readonly ServerStorage.StorageOnTest storage;
		public ServerOnTest()
		{
			storage = new ServerStorage.StorageOnTest();
			Server = new DefaultServer(storage.Storage);
		}
		public ServerOnTest(string backupFile)
		{
			storage = new ServerStorage.StorageOnTest(backupFile);
			Server = new DefaultServer(storage.Storage);
		}
		public void Dispose()
		{
			storage.Dispose();
		}
	}
}
