﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using GroupCheck.ServerStorage;

namespace GroupCheck.UnitTests.ServerStorage
{
	public class StorageOnTest : IDisposable
	{
		private const string host = ".";
		private const string user = "sa";
		private const string pass = "Aw34eszxc";
		private const string db = "groupcheck";

		private const string backups = "C:\\SQL\\_Backups";
		private const string defaultBackup = "empty";

		public readonly IServerStorage Storage;
		private readonly string snapshot;
		private readonly bool fromBackup;
		public StorageOnTest()
		{
			fromBackup = false;
			Storage = new MSSQLStorage(host, user, pass, db);
			snapshot = Guid.NewGuid().ToString();
			((MSSQLStorage)Storage).SysCreateSnapshot(snapshot, backups);
		}
		public StorageOnTest(string backupFile)
		{
			fromBackup = true;
			var newDb = System.IO.Path.GetFileNameWithoutExtension(backupFile);
			var master = new MSSQLStorage(host, user, pass, "master");
			master.SysRestoreBackup(newDb, backupFile, backups);

			Storage = new MSSQLStorage(host, user, pass, newDb);
		}

		public void CreateBackUp(string backupFile)
		{
			((MSSQLStorage)Storage).SysCreateBackup(backupFile);
		}

		public void Dispose()
		{
			if (fromBackup)
			{
			}
			else
			{
				((MSSQLStorage)Storage).SysRestoreSnapshot(snapshot);
				((MSSQLStorage)Storage).SysDeleteSnapshot(snapshot);
			}
		}
	}
}
