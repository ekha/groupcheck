﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using GroupCheck.ServerEntities;
using GroupCheck.ServerStorage;
using GroupCheck.Tools.Extensions;

namespace GroupCheck.UnitTests.ServerStorage
{
	[TestFixture]
	public class Accounts
	{
		[Test, Order(0)]
		public void CRUD()
		{
			using (var target = new StorageOnTest())
			{
				var storage = target.Storage;
				var STEP = "";

				var account = new Account()
				{
					Name = "test account",
					Email = "test.account@sample.com",
					Password = "password",
					Salt = "salt",
					Created = new DateTime(1985, 8, 16, 1, 2, 3),
					Updated = new DateTime(2019, 4, 12, 13, 30, 0),
					Deleted = false
				};

				//==================================================================
				STEP = "CREATE Account";
				var created = storage.Accounts.Create(account.CreateCopy());
				Assert.IsNotNull(created, Messages.NOT_CREATED);
				account.ID = created.ID;
				account.Revision = created.Revision;
				ServerEntitiesValidatior.ValidateAccounts(created, account, STEP);

				//==================================================================
				STEP = "GET Account";
				var fromDb = storage.Accounts.Get(account.ID);
				Assert.IsNotNull(fromDb, Messages.NOT_FOUND);
				ServerEntitiesValidatior.ValidateAccounts(fromDb, account, STEP);

				//==================================================================
				STEP = "UPDATE Account";
				var toUpdate = fromDb.CreateCopy();
				toUpdate.Name = "UPDATED";
				toUpdate.Password = "UPDATED";
				toUpdate.Salt = "UPDATED";
				toUpdate.Created = new DateTime(1944, 9, 13, 4, 5, 6);
				toUpdate.Updated = new DateTime(1955, 5, 14, 7, 8, 9);
				toUpdate.Deleted = true;

				var updated = storage.Accounts.Update(toUpdate.CreateCopy());
				Assert.IsNotNull(updated, Messages.NOT_UPDATED);
				ServerEntitiesValidatior.ValidateRevision(updated.Revision, toUpdate.Revision, STEP);
				ServerEntitiesValidatior.ValidateAccounts(updated, toUpdate, STEP);

				//==================================================================
				STEP = "RESTORE Account";
				var restored = storage.Accounts.Update(account.CreateCopy());
				Assert.IsNotNull(updated, Messages.NOT_RESTORED);
				ServerEntitiesValidatior.ValidateRevision(restored.Revision, updated.Revision, STEP);
				ServerEntitiesValidatior.ValidateAccounts(restored, account, STEP);

				//==================================================================
				STEP = "DELETE Account";
				storage.Accounts.Delete(account.ID);
				var deleted = storage.Accounts.Get(account.ID);
				Assert.IsNull(deleted, Messages.NOT_DELETED);
			}
		}

		[Test, Order(1)]
		public void UpdateAccountField()
		{
			using (var target = new StorageOnTest())
			{
				var storage = target.Storage;

				var account = storage.Accounts.Create(new Account()
				{
					Name = "test account",
					Email = "test.account@sample.com",
					Password = "password",
					Salt = "salt",
					Created = new DateTime(1985, 8, 16, 1, 2, 3),
					Updated = new DateTime(2019, 4, 12, 13, 30, 0),
					Deleted = false
				});

				UpdateAccountField(storage, account.ID, nameof(Account.Name), "UPDATED");
				UpdateAccountField(storage, account.ID, nameof(Account.Email), "UPDATED");
				UpdateAccountField(storage, account.ID, nameof(Account.Password), "UPDATED");
				UpdateAccountField(storage, account.ID, nameof(Account.Salt), "UPDATED");
				UpdateAccountField(storage, account.ID, nameof(Account.Created), new DateTime(2000, 1, 1, 1, 2, 3));
				UpdateAccountField(storage, account.ID, nameof(Account.Updated), new DateTime(2003, 1, 1, 1, 2, 3));
				UpdateAccountField(storage, account.ID, nameof(Account.Deleted), true);
			}
		}
		private void UpdateAccountField(IServerStorage storage, int accountID, string field, object value)
		{
			var account = storage.Accounts.Get(accountID);
			var revision = storage.Accounts.UpdateField(account.ID, field, value);
			ServerEntitiesValidatior.ValidateRevision(revision, account.Revision, $"UPDATE {field}");

			var updated = storage.Accounts.Get(account.ID);
			Assert.AreEqual(revision, updated.Revision, $"Update {field} Revision is different from db");
			Assert.AreEqual(value, typeof(Account).GetProperty(field).GetValue(updated), $"{field} not updated");

			typeof(Account).GetProperty(field).SetValue(account, value);
			ServerEntitiesValidatior.ValidateAccounts(account, updated, "UpdateField {field}");
		}
	}
}
