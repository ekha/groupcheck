﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using GroupCheck.ServerEntities;
using GroupCheck.Server;
using GroupCheck.Tools.Extensions;
using GroupCheck.UnitTests.ServerStorage;

namespace GroupCheck.UnitTests.Server
{
	[TestFixture]
	public class Groups
	{
		[Test, Order(0)]
		public void CRUD()
		{
			using (var target = new ServerOnTest())
			{
				var server = target.Server;
				var STEP = "";

				var account = server.Accounts.Register(new NewAccount() { Email = "admin@sample.com", Password = "123" });
				server.SetCurrentAccount(account.ID);

				//==================================================================
				STEP = "CREATE Group";
				var newGroup = new NewGroup()
				{
					Name = "test group",
					Members = new List<NewMember>()
				};
				var group = server.Groups.Create(newGroup.CreateCopy());
				Assert.IsNotNull(group, Messages.NOT_CREATED);
				Assert.Greater(group.ID, 0, "Created ID == 0");
				Assert.Greater(group.Revision, 0, "Created Revision == 0");
				Assert.AreEqual(newGroup.Name, group.Name, STEP);
				Assert.AreEqual(false, group.Confirmed, STEP);

				//==================================================================
				STEP = "CONFIRM Group By ID";
				var confirmed = server.Groups.Confirm(group.ID);
				Assert.IsNotNull(confirmed, Messages.NOT_CONFIRMED);
				Assert.AreEqual(true, confirmed.Confirmed, Messages.NOT_CONFIRMED);
				group.Confirmed = true;
				group.Revision = confirmed.Revision;
				group.Updated = confirmed.Updated;
				ServerEntitiesValidatior.ValidateGroups(confirmed, group, STEP);

				//==================================================================
				STEP = "GET Group By ID";
				var fromDb = server.Groups.Get(group.ID);
				Assert.IsNotNull(fromDb, STEP + Messages.NOT_FOUND);
				ServerEntitiesValidatior.ValidateGroups(fromDb, group, STEP);

				//==================================================================
				STEP = "UPDATE Group";
				var toUpdate = new UpdateGroup() { ID = group.ID, Name = "UPDATED" };
				var updated = server.Groups.Update(toUpdate.CreateCopy());
				Assert.IsNotNull(updated, Messages.NOT_UPDATED);
				ServerEntitiesValidatior.ValidateRevision(updated.Revision, group.Revision, STEP);
				ServerEntitiesValidatior.ValidateUpdated(updated.Updated, group.Updated, STEP);
				group.Name = "UPDATED";
				group.Revision = updated.Revision;
				group.Updated = updated.Updated;
				ServerEntitiesValidatior.ValidateGroups(updated, group, STEP);

				//==================================================================
				STEP = "DELETE Group";
				server.Groups.Delete(group.ID);
				Assert.Throws<DeletedException>(() => { var acc = server.Groups.Get(group.ID); }, Messages.NOT_DELETED);
			}
		}

		[Test, Order(1)]
		public void CreateWithMembers()
		{
			using (var target = new ServerOnTest())
			{
				var server = target.Server;
				var account = server.Accounts.Register(new NewAccount() { Email = "admin@sample.com", Password = "123" });
				server.SetCurrentAccount(account.ID);

				var member1 = new NewMember() { AccountID = account.ID, GroupID = -4, Name = "admin" };
				var member2 = new NewMember() { AccountID = null, GroupID = 0, Name = "member 2" };
				var member3 = new NewMember() { AccountID = null, GroupID = 0, Name = "member 3" };

				var group = server.Groups.Create(
					new NewGroup()
					{
						Name = "test group",
						Members = new List<NewMember>() { member1, member2, member3 }
					});

				Assert.IsNotNull(group.Members, "Members null");
				Assert.AreEqual(3, group.Members.Count(), "Members count");

				var found1 = group.Members.Single(m => m.Name == member1.Name);
				Assert.IsNotNull(found1, "Member not found");
				Assert.AreEqual(member1.AccountID, found1.AccountID, "Invalid member account ID");
				Assert.AreEqual(group.ID, found1.GroupID, "Invalid member account ID");

				var found2 = group.Members.Single(m => m.Name == member2.Name);
				Assert.IsNotNull(found2, "Member not found");
				Assert.AreEqual(member2.AccountID, found2.AccountID, "Invalid member account ID");
				Assert.AreEqual(group.ID, found2.GroupID, "Invalid member account ID");

				var found3 = group.Members.Single(m => m.Name == member3.Name);
				Assert.IsNotNull(found3, "Member not found");
				Assert.AreEqual(member3.AccountID, found3.AccountID, "Invalid member account ID");
				Assert.AreEqual(group.ID, found3.GroupID, "Invalid member account ID");
			}
		}

		[Test, Order(2)]
		public void AccessToNonConfirmed()
		{
			using (var target = new ServerOnTest())
			{
				var server = target.Server;
				var account = server.Accounts.Register(new NewAccount() { Email = "admin@sample.com", Password = "123" });
				server.SetCurrentAccount(account.ID);

				var group = server.Groups.Create(new NewGroup() { Name = "test group" });

				Assert.Throws<NotFoundException>(() => { server.Groups.Get(group.ID); }, "Get");
				Assert.Throws<NotFoundException>(() => { server.Groups.Update(new UpdateGroup() { ID = group.ID, Name = "update" }); }, "Update");
				Assert.Throws<NotFoundException>(() => { server.Groups.Delete(group.ID); }, "Delete");
			}
		}

		[Test, Order(3)]
		public void ConfirmByNonAuthor()
		{
			using (var target = new ServerOnTest())
			{
				var server = target.Server;

				var author = server.Accounts.Register(new NewAccount() { Email = "admin@sample.com", Password = "123" });
				server.SetCurrentAccount(author.ID);

				var group = server.Groups.Create(new NewGroup() { Name = "test group" });

				var user = server.Accounts.Register(new NewAccount() { Email = "user@sample.com", Password = "123" });
				server.SetCurrentAccount(user.ID);

				Assert.Throws<AccessDeniedException>(() => { server.Groups.Confirm(group.ID); } );
			}
		}
	}
}
