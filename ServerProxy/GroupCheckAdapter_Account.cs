﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;
using GroupCheck.WebApi;
using GroupCheck.Tools.Extensions;

namespace GroupCheck.ServerProxy
{
	public partial class GroupCheckAdapter : IAccountAPI
	{
		RegisterResponse IAccountAPI.Register(NewAccount request)
		{
			var url = $"{_host}/api/Account/Register";
			var body = request.ToJSON();
			var response = HTTP.MakeRequest("POST", url, CONTENT_TYPE_JSON, null, body);

			if (response.ResponseCode == HttpStatusCode.OK)
			{
				var created = response.Content.ParseJSON<RegisterResponse>();

				_token = created.Token;
				_email = created.Email;
				_password = request.Password;
				_accountID = created.AccountID;

				return created;
			}
			else
				throw new Exception(response.Content);
		}
		GetAccountResponse IAccountAPI.GetAccount()
		{
			var url = $"/api/Account/GetAccount";
			var response = Get(url);
			if (response.ResponseCode == HttpStatusCode.OK)
			{
				return response.Content.ParseJSON<GetAccountResponse>();
			}
			else
				throw new Exception(response.Content);
		}
		GetTokenResponse IAccountAPI.GetToken(GetTokenRequest request)
		{
			var url = $"{_host}/api/Account/GetToken";
			var body = new GetTokenRequest() { Email = request.Email, Password = request.Password }.ToJSON();
			var response = HTTP.MakeRequest("POST", url, CONTENT_TYPE_JSON, null, body);
			if (response.ResponseCode == HttpStatusCode.OK)
			{
				return response.Content.ParseJSON<GetTokenResponse>();
			}
			else
				throw new Exception(response.Content);
		}
		void IAccountAPI.ChangePassword(ChangePasswordRequest request)
		{
			var url = $"/api/Account/ChangePassword";
			var body = request.ToJSON();
			var response = Post(url, body);

			if (response.ResponseCode == HttpStatusCode.NoContent)
			{
				return;
			}
			else
				throw new Exception(response.Content);
		}
	}
}
