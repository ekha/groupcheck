﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;
using GroupCheck.WebApi;
using GroupCheck.Tools.Extensions;

namespace GroupCheck.ServerProxy
{
	public partial class GroupCheckAdapter : IGroupAPI
	{
		Group IGroupAPI.Create(NewGroup request)
		{
			var created = Post<Group>("/api/Group/Create", request);
			return created;
		}
		Group IGroupAPI.Confirm(int id)
		{
			var created = Get<Group>($"/api/Group/Confirm/{id}");
			return created;
		}
		Group IGroupAPI.Update(UpdateGroup request)
		{
			var updated = Post<Group>($"/api/Group/Update", request);
			return updated;
		}
	}
}
