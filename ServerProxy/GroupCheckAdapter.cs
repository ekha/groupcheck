﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.IO;
using GroupCheck.WebApi;
using GroupCheck.Tools.Extensions;

namespace GroupCheck.ServerProxy
{
	public partial class GroupCheckAdapter : IAccountAPI, IGroupAPI, IMemberAPI, ICheckAPI
	{
		#region constants
		private const string CONTENT_TYPE_JSON = "application/json";
		private const int MAX_REQUESTS_PER_SECOND = 50;
		private const int TIMEOUT_BEFORE_RETRY_REQUEST = 1000;
		private const int TIMEOUT_BEFORE_RETRY_AUTH = 10;
		private const int HTTP_TOO_MANY_REQUESTS = 429;
		private const int GATEWAY_TIMEOUT = 502;
		private const int DEFAULT_BATCH_SIZE = 50;
		#endregion

		private readonly string _host = "http://localhost/gc";
		private string _email;
		private string _password;
		private int _accountID;
		private string _token;

		public IAccountAPI Accounts { get { return this; } }
		public IGroupAPI Groups { get { return this; } }
		public IMemberAPI Members { get { return this; } }
		public ICheckAPI Checks { get { return this; } }

		public GroupCheckAdapter(string host)
		{
			_host = host;
		}

		public void Login(string email, string password)
		{
			_token = null;
			_email = email;
			_password = password;

			var response = Accounts.GetToken(new GetTokenRequest() { Email = this._email, Password = password });
			_accountID = response.AccountID;
			_token = response.Token;
		}

		#region REST
		public T Get<T>(string path)
		{
			var response = Get(path);
			if (response.ResponseCode == HttpStatusCode.OK)
			{
				var result = response.Content.ParseJSON<T>();
				return result;
			}
			else
			{
				throw new Exception(response.Content);
			}
		}
		public T Post<T>(string path, object body)
		{
			return Post<T>(path, body.ToJSON());
		}
		public T Post<T>(string path, string body)
		{
			var response = Post(path, body);
			if (response.ResponseCode == HttpStatusCode.OK)
			{
				var result = response.Content.ParseJSON<T>();
				return result;
			}
			else
			{
				throw new Exception(response.Content);
			}
		}

		public HttpResponse Get(string path)
		{
			return SafeRequest("GET", path);
		}
		public HttpResponse Post(string path, string body)
		{
			return SafeRequest("POST", path, body);
		}
		public HttpResponse Put(string path, string body)
		{
			return SafeRequest("PUT", path, body);
		}
		public HttpResponse Delete(string path)
		{
			return SafeRequest("DELETE", path);
		}

		private HttpResponse SafeRequest(string method, string path, string body = null)
		{
			WaitSecondlyLimit();

			PrepareRequest(path, out string url, out Dictionary<string, string> headers);

			var response = HTTP.MakeRequest(method, url, CONTENT_TYPE_JSON, headers, body);

			if (response.ResponseCode == HttpStatusCode.OK ||
				response.ResponseCode == HttpStatusCode.NoContent ||
				response.ResponseCode == HttpStatusCode.Accepted)
				return response;

			switch (response.ResponseCode)
			{
				case HttpStatusCode.Unauthorized:
				case HttpStatusCode.Forbidden:
					response.ErrorMessage = response.Content;
					_token = null;
					break;

				case HttpStatusCode.BadRequest:
					response.ErrorMessage = response.Content;
					break;
			}

			return response;
		}
		private void PrepareRequest(string path, out string url, out Dictionary<string, string> headers)
		{
			path = path.TrimStart('/');
			url = $"{_host}/{path}";
			headers = new Dictionary<string, string>();

			if (string.IsNullOrEmpty(_token))
			{
				Authenticate();
			}
			headers["Authorization"] = $"Bearer {_token}";
		}

		private void Authenticate()
		{
			if (string.IsNullOrEmpty(_email) || string.IsNullOrEmpty(_password))
				throw new Exception("LOGIN FIRST!!!");

			lock (_lastRequests)
			{
				var response = Accounts.GetToken(new GetTokenRequest() { Email = _email, Password = _password });
				_accountID = response.AccountID;
				_token = response.Token;
			}
		}
		#endregion

		#region Adapter State
		private readonly List<DateTime> _lastRequests = new List<DateTime>();
		private void WaitSecondlyLimit()
		{
			lock (_lastRequests)
			{
				_lastRequests.RemoveAll(time => time <= DateTime.Now.AddSeconds(-1));
				if (_lastRequests.Count >= MAX_REQUESTS_PER_SECOND)
				{
					var minTime = _lastRequests.Min();
					var waitTime = (DateTime.Now - minTime).TotalMilliseconds;
					System.Threading.Thread.Sleep((int)waitTime);
				}
				_lastRequests.Add(DateTime.Now);
			}
		}
		#endregion
	}
}
