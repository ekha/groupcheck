﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;
using GroupCheck.WebApi;
using GroupCheck.Tools.Extensions;

namespace GroupCheck.ServerProxy
{
	public partial class GroupCheckAdapter : IMemberAPI
	{
		Member IMemberAPI.Create(NewMember request)
		{
			var created = Post<Member>("/api/Member/Create", request);
			return created;
		}
		Member IMemberAPI.Confirm(int groupID, int memberID)
		{
			var created = Get<Member>($"/api/Member/Confirm/{groupID}/{memberID}");
			return created;
		}
		Member IMemberAPI.Update(UpdateMember request)
		{
			var updated = Post<Member>($"/api/Member/Update", request);
			return updated;
		}
	}
}
