﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;
using GroupCheck.WebApi;
using GroupCheck.Tools.Extensions;

namespace GroupCheck.ServerProxy
{
	public partial class GroupCheckAdapter : ICheckAPI
	{
		Check ICheckAPI.Create(NewCheck request)
		{
			var created = Post<Check>("/api/Check/Create", request);
			return created;
		}
		Check ICheckAPI.Confirm(int groupID, int id)
		{
			var created = Get<Check>($"/api/Check/Confirm/{groupID}/{id}");
			return created;
		}
		Check ICheckAPI.Update(UpdateCheck request)
		{
			var updated = Post<Check>($"/api/Check/Update", request);
			return updated;
		}
	}
}
