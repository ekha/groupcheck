﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GroupCheck.ServerEntities;
using GroupCheck.ServerStorage;
using System.Threading;

namespace GroupCheck.Server
{
	public partial class DefaultServer : IServer, IServerAccounts, IServerGroups, IServerMembers, IServerChecks
	{
		private readonly IServerStorage storage;

		private const int ANONYMOUS = 0;
		private int _currentAccountID;
		public int CurrentAccountID { get { return _currentAccountID; } }

		public DefaultServer(IServerStorage storage) : this(ANONYMOUS, storage) { }
		public DefaultServer(int accountID, IServerStorage storage)
		{

			_currentAccountID = accountID;
			this.storage = storage;

			for(var i = 0; i < lockers.Length; i++)
			{
				lockers[i] = new object();
			}
		}

		public void SetCurrentAccount(int accountID)
		{
			_currentAccountID = accountID;
		}
		public void WorkAsAnonymous()
		{
			_currentAccountID = ANONYMOUS;
		}
		public bool IsAnonymous { get { return _currentAccountID == ANONYMOUS; } }

		public IServerAccounts Accounts { get { return this; } }
		public IServerGroups Groups { get { return this; } }
		public IServerMembers Members { get { return this; } }
		public IServerChecks Checks { get { return this; } }

		private DateTime Now()
		{
			return DateTime.UtcNow;
		}

		private static readonly object[] lockers = new object[10000];
		private object GetGroupLocker(int groupID)
		{
			return lockers[groupID % lockers.Length];
		}
	}
}
