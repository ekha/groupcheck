﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GroupCheck.ServerEntities;
using GroupCheck.Tools;
using GroupCheck.Tools.Extensions;

namespace GroupCheck.Server
{
	public partial class DefaultServer
	{
		Check IServerChecks.Create(NewCheck newCheck)
		{
			RequireNotNull(newCheck);
			RequireAuthentication();

			lock (GetGroupLocker(newCheck.GroupID))
			{
				var group = storage.Groups.Get(newCheck.GroupID, false);
				if (group == null || !group.Confirmed)
					throw new NotFoundException();

				var checkID = storage.Groups.GetLastCheckID(group.ID) + 1;

				var check = new Check()
				{
					GroupID = newCheck.GroupID,
					CheckID = checkID,
					DateAndTime = newCheck.DateAndTime,
					Description = newCheck.Description,
					Creditors = newCheck.Creditors,
					Debitors = newCheck.Debitors,

					Created = Now(),
					CreatedByID = CurrentAccountID,
					Updated = Now(),
					UpdatedByID = CurrentAccountID,
				};
				check = storage.Checks.Create(check);
				return check;
			}
		}

		Check IServerChecks.Confirm(int groupID, int checkID)
		{
			RequireAuthentication();

			var check = storage.Checks.Get(groupID, checkID);
			if (check == null)
				throw new NotFoundException();

			if (check.CreatedByID != CurrentAccountID)
				throw new AccessDeniedException();

			if (check.Confirmed)
				return check;

			check.Confirmed = true;
			check.Updated = Now();

			check = storage.Checks.Update(check);
			return check;
		}

		Check IServerChecks.Get(int groupID, int checkID)
		{
			var check = storage.Checks.Get(groupID, checkID);

			if (check == null || !check.Confirmed)
				throw new NotFoundException();

			if (check.Deleted)
				throw new DeletedException();

			return check;
		}

		Check IServerChecks.Update(UpdateCheck updateCheck)
		{
			RequireNotNull(updateCheck);
			RequireAuthentication();

			var check = storage.Checks.Get(updateCheck.GroupID, updateCheck.CheckID);

			if (check == null || !check.Confirmed)
				throw new NotFoundException();

			if (check.Deleted)
				throw new DeletedException();

			check.DateAndTime = updateCheck.DateAndTime;
			check.Description = updateCheck.Description;
			check.Debitors = updateCheck.Debitors;
			check.Creditors = check.Creditors;

			check.Updated = Now();
			check.UpdatedByID = CurrentAccountID;

			check = storage.Checks.Update(check);
			return check;
		}

		void IServerChecks.Delete(int groupID, int checkID)
		{
			RequireAuthentication();

			var check = storage.Checks.Get(groupID, checkID);

			if (check == null || !check.Confirmed)
				throw new NotFoundException();

			if (check.Deleted)
				throw new DeletedException();

			var revision = storage.Checks.UpdateField(check.GroupID, check.CheckID, nameof(Member.Deleted), true);
		}
	}
}
