﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GroupCheck.ServerEntities;
using GroupCheck.ServerStorage;
using System.Threading;

namespace GroupCheck.Server
{
	public partial class DefaultServer
	{
		private void RequireAuthentication()
		{
			if (_currentAccountID == ANONYMOUS)
				throw new AuthenticationRequiredException();
		}

		private void RequireNotNull(object value)
		{
			if (value == null)
				throw new ArgumentNullException();
		}
	}
}
