﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GroupCheck.ServerEntities;
using GroupCheck.Tools;
using GroupCheck.Tools.Extensions;

namespace GroupCheck.Server
{
	public partial class DefaultServer
	{
		Group IServerGroups.Create(NewGroup newGroup)
		{
			RequireNotNull(newGroup);
			RequireAuthentication();

			if (string.IsNullOrEmpty(newGroup.Name))
				throw new ValidationException(ValidationCode.GroupEmptyName);

			var group = ParseNewGroup(newGroup, Now());
			if (group.Members != null && group.Members.Any())
			{
				var members = group.Members.ToArray();
				for (int i = 0; i < members.Length; i++)
				{
					members[i].MemberID = i + 1;
				}
				group.Members = members;
			}
			
			group = storage.Groups.Create(group);
			return group;
		}

		Group IServerGroups.Confirm(int groupID)
		{
			RequireAuthentication();

			var group = storage.Groups.Get(groupID, true);

			if (group == null)
				throw new NotFoundException();

			if (group.CreatedByID != CurrentAccountID)
				throw new AccessDeniedException();

			if (group.Confirmed) return group;

			group.Confirmed = true;
			group.Updated = Now();

			group = storage.Groups.Update(group);
			group = storage.Groups.Get(groupID, true);
			return group;
		}

		Group IServerGroups.Get(int groupID)
		{
			var group = storage.Groups.Get(groupID, true);

			if (group == null || !group.Confirmed)
				throw new NotFoundException();

			if (group.Deleted)
				throw new DeletedException();

			return group;
		}

		Group IServerGroups.Update(UpdateGroup updateGroup)
		{
			RequireNotNull(updateGroup);
			RequireAuthentication();

			if (string.IsNullOrEmpty(updateGroup.Name))
				throw new ValidationException(ValidationCode.GroupEmptyName);

			var group = storage.Groups.Get(updateGroup.ID, false);

			if (group == null || !group.Confirmed)
				throw new NotFoundException();

			if (group.Deleted)
				throw new DeletedException();

			if (group.Name == updateGroup.Name)
				return group;

			group.Name = updateGroup.Name;

			group.Updated = Now();
			group.UpdatedByID = CurrentAccountID;

			group = storage.Groups.Update(group);
			return group;
		}

		void IServerGroups.Delete(int groupID)
		{
			RequireAuthentication();

			var group = storage.Groups.Get(groupID, false);

			if (group == null || !group.Confirmed)
				throw new NotFoundException();

			if (group.Deleted)
				throw new DeletedException();

			var revision = storage.Groups.UpdateField(group.ID, nameof(Group.Deleted), true);
		}



		internal Group ParseNewGroup(NewGroup newGroup, DateTime now)
		{
			return new Group()
			{
				Name = newGroup.Name,

				Created = now,
				CreatedByID = CurrentAccountID,
				Updated = now,
				UpdatedByID = CurrentAccountID,

				Members = newGroup.Members?.Select(m => ParseNewMember(m, now))
			};
		}
	}
}
