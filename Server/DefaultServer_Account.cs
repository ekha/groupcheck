﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GroupCheck.ServerEntities;
using GroupCheck.Tools;
using GroupCheck.Tools.Extensions;

namespace GroupCheck.Server
{
	public partial class DefaultServer
	{
		private const string PASSWORD_SECRET_SALT = "3qM8Za4HzEOygASmk+78Pj0n4AEw/AzibEaCg2tjRns=";

		public static string GetPass(string pass, string salt)
		{
			return pass.CalculateHash(salt, PASSWORD_SECRET_SALT);
		}

		Account IServerAccounts.Register(NewAccount newAccount)
		{
			RequireNotNull(newAccount);

			if (string.IsNullOrEmpty(newAccount.Email))
				throw new ValidationException(ValidationCode.AccountEmptyEmail);

			if (!Email.TryParse(newAccount.Email, out string parsedEmail, out string parsedName))
				throw new ValidationException(ValidationCode.AccountInvalidEmail);

			newAccount.Email = parsedEmail;
			newAccount.Name = newAccount.Name ?? parsedName ?? string.Empty;
			newAccount.Password = newAccount.Password ?? Password.GeneratePassword(8);

			var existing = storage.Accounts.Find(newAccount.Email);
			if (existing != null)
				throw new AlreadyExistsException();

			var salt = Password.GeneratePassword(16);
			var hash = newAccount.Password.CalculateHash(salt, PASSWORD_SECRET_SALT);

			var account = new Account()
			{
				Email = newAccount.Email,
				Name = newAccount.Name,
				Salt = salt,
				Password = hash,

				Created = Now(),
				Updated = Now(),
			};

			account = storage.Accounts.Create(account);
			return account;
		}
		Account IServerAccounts.GetCurrent()
		{
			RequireAuthentication();

			var account = storage.Accounts.Get(CurrentAccountID);
			if (account == null)
				throw new NotFoundException();

			if (account.Deleted)
				throw new DeletedException();

			return account;
		}
		Account IServerAccounts.Get(int id)
		{
			RequireAuthentication();

			var account = storage.Accounts.Get(id);

			if (account == null)
				throw new NotFoundException();

			if (account.Deleted)
				throw new DeletedException();

			return account;
		}
		Account IServerAccounts.Update(UpdateAccount updateAccount)
		{
			RequireNotNull(updateAccount);
			RequireAuthentication();

			var orig = this.Accounts.GetCurrent();
			if (orig == null) return null;

			orig.Name = updateAccount.Name;

			orig.Updated = Now();

			orig = storage.Accounts.Update(orig);
			return orig;
		}
		void IServerAccounts.Delete()
		{
			RequireAuthentication();

			var account = storage.Accounts.Get(CurrentAccountID);

			if (account == null)
				throw new NotFoundException();

			if (account.Deleted)
				throw new DeletedException();

			var revision = storage.Accounts.UpdateField(CurrentAccountID, nameof(ServerEntities.Account.Deleted), true);
		}
		Account IServerAccounts.LoginByEmail(string email, string password)
		{
			var account = storage.Accounts.Find(email);
			if (account == null)
				throw new ValidationException(ValidationCode.AccountInvalidEmail);
			if (account.Password.Trim() != string.Empty && account.Password != password.CalculateHash(account.Salt, PASSWORD_SECRET_SALT))
				throw new ValidationException(ValidationCode.AccountInvalidPassword);
			return account;
		}
		Account IServerAccounts.FindByEmail(string email)
		{
			RequireAuthentication();

			if (string.IsNullOrEmpty(email))
				throw new ValidationException(ValidationCode.AccountEmptyEmail);

			if (!Email.TryParse(email, out string parsedEmail, out string parsedName))
				throw new ValidationException(ValidationCode.AccountInvalidEmail);

			return storage.Accounts.Find(email);
		}
		Account IServerAccounts.ChangePassword(string password)
		{
			RequireAuthentication();

			var account = this.Accounts.GetCurrent();
			if (account == null) return null;

			account.Salt = Password.GeneratePassword(16);
			account.Password = password.CalculateHash(account.Salt, PASSWORD_SECRET_SALT);
			account.Updated = Now();
			account = storage.Accounts.Update(account);

			return account;
		}
	}
}
