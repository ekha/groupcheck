﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GroupCheck.ServerEntities;
using GroupCheck.Tools;
using GroupCheck.Tools.Extensions;

namespace GroupCheck.Server
{
	public partial class DefaultServer
	{
		Member IServerMembers.Create(NewMember newMember)
		{
			RequireNotNull(newMember);
			RequireAuthentication();

			if (string.IsNullOrWhiteSpace(newMember.Name))
				throw new ValidationException(ValidationCode.GroupEmptyName);

			lock (GetGroupLocker(newMember.GroupID))
			{
				var group = storage.Groups.Get(newMember.GroupID, false);
				if (group == null || !group.Confirmed)
					throw new NotFoundException();

				var memberID = storage.Groups.GetLastMemberID(group.ID) + 1;

				var member = ParseNewMember(newMember, Now());
				member.MemberID = memberID;
				member = storage.Members.Create(member);
				return member;
			}
		}

		Member IServerMembers.Confirm(int groupID, int memberID)
		{
			RequireAuthentication();

			var member = storage.Members.Get(groupID, memberID);
			if (member == null)
				throw new NotFoundException();

			if (member.CreatedByID != CurrentAccountID)
				throw new AccessDeniedException();

			if (member.Confirmed)
				return member;

			member.Confirmed = true;
			member.Updated = Now();

			member = storage.Members.Update(member);
			return member;
		}

		Member IServerMembers.Get(int groupID, int memberID)
		{
			var member = storage.Members.Get(groupID, memberID);

			if (member == null || !member.Confirmed)
				throw new NotFoundException();

			if (member.Deleted)
				throw new DeletedException();

			return member;
		}

		Member IServerMembers.Update(UpdateMember updateMember)
		{
			RequireNotNull(updateMember);
			RequireAuthentication();

			if (string.IsNullOrWhiteSpace(updateMember.Name))
				throw new ValidationException(ValidationCode.MemberEmptyName);

			var member = storage.Members.Get(updateMember.GroupID, updateMember.MemberID);

			if (member == null || !member.Confirmed)
				throw new NotFoundException();

			if (member.Deleted)
				throw new DeletedException();

			if (member.Name == updateMember.Name && member.AccountID == updateMember.AccountID)
				return member;

			member.Name = updateMember.Name;
			member.AccountID = updateMember.AccountID;

			member.Updated = Now();
			member.UpdatedByID = CurrentAccountID;

			member = storage.Members.Update(member);
			return member;
		}

		void IServerMembers.Delete(int groupID, int memberID)
		{
			RequireAuthentication();

			var member = storage.Members.Get(groupID, memberID);

			if (member == null || !member.Confirmed)
				throw new NotFoundException();

			if (member.Deleted)
				throw new DeletedException();

			var revision = storage.Members.UpdateField(member.GroupID, member.MemberID, nameof(Member.Deleted), true);
		}



		internal Member ParseNewMember(NewMember newMember, DateTime now)
		{
			return new Member()
			{
				GroupID = newMember.GroupID,
				AccountID = newMember.AccountID,
				Name = newMember.Name,

				Created = now,
				CreatedByID = CurrentAccountID,
				Updated = now,
				UpdatedByID = CurrentAccountID,
			};
		}
	}
}
