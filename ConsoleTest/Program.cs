﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GroupCheck.Tools;
using System.Diagnostics;
using GroupCheck.Server;
using GroupCheck.ServerEntities;
using GroupCheck.ServerStorage;
using System.Threading;
using GroupCheck.WebApi;
using GroupCheck.Tools.Extensions;
namespace ConsoleTest
{
	class Program
	{
		static void Main(string[] args)
		{
			ApiChecks();
		}

		static void BigDb()
		{
			var sw = new Stopwatch();
			sw.Start();
			var db = new BigDatabase(".", "sa", "Aw34eszxc", "gc_medium");
			db.Fill(1000000, 1000000, 10 * 1000000, 100 * 1000000);
			sw.Stop();
			Console.WriteLine(sw.Elapsed);
		}

		static void ApiAccount()
		{
			var api = new GroupCheck.ServerProxy.GroupCheckAdapter("http://localhost/gc");
			api.Accounts.Register(new GroupCheck.WebApi.NewAccount() { Email = "admin@sample.com", Name = "name", Password = "123" });
			api.Accounts.GetToken(new GetTokenRequest() { Email = "admin@sample.com", Password = "123" });
			api.Accounts.ChangePassword(new ChangePasswordRequest() { Password = "321" });
		}

		static void ApiChecks()
		{
			var api = new GroupCheck.ServerProxy.GroupCheckAdapter("http://localhost/gc");
			var acc = api.Accounts.Register(new GroupCheck.WebApi.NewAccount() { Email = "admin@mail.ru", Password = "123" });
			api.Checks.Confirm(15, 25);
		}
	}
}

