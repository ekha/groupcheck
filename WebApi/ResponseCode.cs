﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GroupCheck.WebApi
{
	public static class ResponseCode
	{
		public const string UNDEFINED_ERROR = "UNDEFINED_ERROR";
		public const string EMPTY_REQUEST = "EMPTY_REQUEST";
		public const string ALREADY_EXISTS = "ALREADY_EXISTS";
		public const string EMPTY_EMAIL = "EMPTY_EMAIL";
		public const string INVALID_EMAIL = "INVALID_EMAIL";
		public const string INVALID_PASSWORD = "INVALID_PASSWORD";
		public const string EMPTY_GROUP_NAME = "EMPTY_GROUP_NAME";
		public const string EMPTY_MEMBER_NAME = "EMPTY_MEMBER_NAME";
	}
}
