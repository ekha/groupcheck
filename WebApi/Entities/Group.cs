﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GroupCheck.WebApi
{
	public interface IGroupAPI
	{
		Group Create(NewGroup request);
		Group Confirm(int id);
		Group Update(UpdateGroup request);
	}

	public class Group
	{
		public int ID { get; set; }
		public string Name { get; set; }
		public IEnumerable<Member> Members { get; set; }

		public DateTime Created { get; set; }
		public int CreatedByID { get; set; }
		public DateTime Updated { get; set; }
		public int UpdatedByID { get; set; }
		public long Revision { get; set; }
	}

	public class NewGroup
	{
		public string Name { get; set; }
		public IEnumerable<NewMember> Members { get; set; }
	}

	public class UpdateGroup
	{
		public int ID { get; set; }
		public string Name { get; set; }
	}
}
