﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GroupCheck.WebApi
{
	public interface ICheckAPI
	{
		Check Create(NewCheck request);
		Check Confirm(int groupID, int checkID);
		Check Update(UpdateCheck request);
	}

	public class Check
	{
		public int GroupID { get; set; }
		public int CheckID { get; set; }
		public DateTime DateAndTime { get; set; }
		public string Description { get; set; }
		public IDictionary<int, int> Debitors { get; set; }
		public IDictionary<int, int> Creditors { get; set; }

		public DateTime Created { get; set; }
		public int CreatedByID { get; set; }
		public DateTime Updated { get; set; }
		public int UpdatedByID { get; set; }
		public long Revision { get; set; }
	}

	public class NewCheck
	{
		public int GroupID { get; set; }
		public DateTime DateAndTime { get; set; }
		public string Description { get; set; }
		public IDictionary<int, int> Debitors { get; set; }
		public IDictionary<int, int> Creditors { get; set; }
	}

	public class UpdateCheck
	{
		public int GroupID { get; set; }
		public int CheckID { get; set; }
		public DateTime DateAndTime { get; set; }
		public string Description { get; set; }
		public IDictionary<int, int> Debitors { get; set; }
		public IDictionary<int, int> Creditors { get; set; }
	}
}
