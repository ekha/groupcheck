﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GroupCheck.WebApi
{
	public interface IMemberAPI
	{
		Member Create(NewMember request);
		Member Confirm(int groupID, int memberID);
		Member Update(UpdateMember request);
	}

	public class Member
	{
		public int GroupID { get; set; }
		public int MemberID { get; set; }
		public string Name { get; set; }
		public int? AccountID { get; set; }

		public DateTime Created { get; set; }
		public int CreatedByID { get; set; }
		public DateTime Updated { get; set; }
		public int UpdatedByID { get; set; }
		public long Revision { get; set; }
	}

	public class NewMember
	{
		public int GroupID { get; set; }
		public string Name { get; set; }
		public int? AccountID { get; set; }
	}

	public class UpdateMember
	{
		public int GroupID { get; set; }
		public int MemberID { get; set; }
		public string Name { get; set; }
		public int? AccountID { get; set; }
	}
}
