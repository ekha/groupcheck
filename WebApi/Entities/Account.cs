﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GroupCheck.WebApi
{
	public interface IAccountAPI
	{
		RegisterResponse Register(NewAccount request);
		GetTokenResponse GetToken(GetTokenRequest request);
		GetAccountResponse GetAccount();
		void ChangePassword(ChangePasswordRequest request);
	}

	public class NewAccount
	{
		public string Email { get; set; }
		public string Name { get; set; }
		public string Password { get; set; }
	}

	public class RegisterResponse
	{
		public int AccountID { get; set; }
		public string Email { get; set; }
		public string Name { get; set; }
		public string Token { get; set; }
		public long Revision { get; set; }
	}

	public class GetAccountResponse
	{
		public int AccountID { get; set; }
		public string Email { get; set; }
		public string Name { get; set; }
		public long Revision { get; set; }
	}

	public class GetTokenRequest
	{
		public string Email { get; set; }
		public string Password { get; set; }
	}

	public class GetTokenResponse
	{
		public int AccountID { get; set; }
		public string Token { get; set; }
	}

	public class ChangePasswordRequest
	{
		public string Password { get; set; }
	}
}
