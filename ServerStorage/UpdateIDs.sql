ALTER TABLE Members ADD MemberID int
GO

with ids as
(
select ID as ID, ROW_NUMBER() over(partition by GroupID order by ID) as MemberID
from Members
)
update m set m.MemberID = tmp.MemberID
from Members as m
join ids as tmp on tmp.id=m.ID
GO


ALTER TABLE Checks ADD CheckID int
GO

with ids as
(
select ID as ID, ROW_NUMBER() over(partition by GroupID order by ID) as CheckID
from Checks
)
update m set m.CheckID = tmp.CheckID
from Checks as m
join ids as tmp on tmp.id=m.ID
GO


CREATE TABLE [dbo].[Members2](
	[GroupID] [int] NOT NULL,
	[MemberID] [int] NOT NULL,
	[AccountID] [int] NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Confirmed] [bit] NOT NULL,
	[Created] [datetime2](7) NOT NULL,
	[CreatedByID] [int] NOT NULL,
	[Updated] [datetime2](7) NOT NULL,
	[UpdatedByID] [int] NOT NULL,
	[Revision] [timestamp] NOT NULL,
	[Deleted] [bit] NOT NULL,
 CONSTRAINT [PK_Members2] PRIMARY KEY CLUSTERED 
(
	[GroupID] ASC, [MemberID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Checks2](
	[GroupID] [int] NOT NULL,
	[CheckID] [int] NOT NULL,
	[DateAndTime] [smalldatetime] NOT NULL,
	[Description] [nvarchar](50) NOT NULL,
	[Creditors] [nvarchar](1000) NOT NULL,
	[Debitors] [nvarchar](1000) NOT NULL,
	[Confirmed] [bit] NOT NULL,
	[Created] [datetime2](7) NOT NULL,
	[CreatedByID] [int] NOT NULL,
	[Updated] [datetime2](7) NOT NULL,
	[UpdatedByID] [int] NOT NULL,
	[Revision] [timestamp] NOT NULL,
	[Deleted] [bit] NOT NULL,
	
 CONSTRAINT [PK_Checks2] PRIMARY KEY CLUSTERED 
(
	[GroupID] ASC, [CheckID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO



INSERT INTO Members2 
(
       [GroupID]
	  ,[MemberID]
      ,[AccountID]
      ,[Name]
      ,[Confirmed]
      ,[Created]
      ,[CreatedByID]
      ,[Updated]
      ,[UpdatedByID]
      ,[Deleted]
)

SELECT [GroupID]
	  ,[MemberID]
      ,[AccountID]
      ,[Name]
      ,[Confirmed]
      ,[Created]
      ,[CreatedByID]
      ,[Updated]
      ,[UpdatedByID]
      ,[Deleted]
  FROM [Members]


  INSERT INTO Checks2(
       [GroupID]
      ,[CheckID]
      ,[DateAndTime]
      ,[Description]
      ,[Creditors]
      ,[Debitors]
      ,[Confirmed]
      ,[Created]
      ,[CreatedByID]
      ,[Updated]
      ,[UpdatedByID]
      ,[Deleted]
)
SELECT [GroupID]
      ,[CheckID]
      ,[DateAndTime]
      ,[Description]
      ,[Creditors]
      ,[Debitors]
      ,[Confirmed]
      ,[Created]
      ,[CreatedByID]
      ,[Updated]
      ,[UpdatedByID]
      ,[Deleted]
  FROM [Checks]


alter table Members drop column MemberID
alter table Checks drop column CheckID

CREATE NONCLUSTERED INDEX [IX_GroupID] ON [Checks]
(
	[GroupID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


CREATE NONCLUSTERED INDEX [IX_GroupID] ON [Members]
(
	[GroupID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


