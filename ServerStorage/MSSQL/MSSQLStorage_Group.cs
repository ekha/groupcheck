﻿using System;
using System.Linq;
using System.Data.SqlClient;
using System.Data;
using System.Collections.Generic;
using GroupCheck.ServerEntities;

namespace GroupCheck.ServerStorage
{
	public partial class MSSQLStorage : IServerStorageGroups
	{
		Group IServerStorageGroups.Create(Group group)
		{
			var cmd = GetCreateSql(group);
			using (var ds = GetDataSet(cmd))
			{
				var created = ParseGroup(ds);
				return created;
			}
		}
		Group IServerStorageGroups.Get(int groupID, bool includeMembers)
		{
			if (includeMembers)
			{
				var cmd = GetGroupWithMembersSql(groupID);
				using (var ds = GetDataSet(cmd))
				{
					var group = ParseGroup(ds);
					return group;
				}
			}
			else
			{
				var cmd = GetReadSql<Group>(groupID);
				var row = GetDataRow(cmd);
				var group = ParseGroup(row);
				return group;
			}
		}
		Group IServerStorageGroups.Update(Group group)
		{
			var cmd = GetUpdateSql(group);
			var row = GetDataRow(cmd);
			var updated = ParseGroup(row);
			return updated;
		}
		long IServerStorageGroups.UpdateField(int groupID, string fieldName, object value)
		{
			var cmd = GetUpdateFieldSql<Group>(groupID, fieldName, value);
			var result = ExecuteScalar(cmd);
			var revision = ParseRevision((byte[])result);
			return revision;
		}
		void IServerStorageGroups.Delete(int groupID)
		{
			var cmd = GetDeleteSql<Group>(groupID);
			Execute(cmd);
		}
		IEnumerable<Group> IServerStorageGroups.GetGroupsByAccount(int accountID, long minRevision, int batchSize)
		{
			var cmd = GetGroupsByAccountSql(accountID, minRevision, batchSize + 1);
			using (var table = GetTable(cmd))
			{
				foreach (DataRow row in table.Rows)
				{
					yield return ParseGroup(row);
				}
			}
		}

		int IServerStorageGroups.GetLastMemberID(int groupID)
		{
			var sql = "SELECT ISNULL(MAX(MemberID), 0) FROM Members WHERE GroupID = @groupID";
			var cmd = new SqlCommand(sql);
			cmd.Parameters.AddWithValue("@groupID", groupID);
			var memberID = (int)ExecuteScalar(cmd);
			return memberID;
		}
		int IServerStorageGroups.GetLastCheckID(int groupID)
		{
			var sql = "SELECT ISNULL(MAX(CheckID), 0) FROM Checks WHERE GroupID = @groupID";
			var cmd = new SqlCommand(sql);
			cmd.Parameters.AddWithValue("@groupID", groupID);
			var checkID = (int)ExecuteScalar(cmd);
			return checkID;
		}

		private Group ParseGroup(DataRow row)
		{
			if (row == null) return null;

			var group = new Group()
			{
				ID = (int)row["ID"],
				Name = (string)row["Name"],
				Members = new List<Member>(),

				Confirmed = (bool)row["Confirmed"],
				Created = (DateTime)row["Created"],
				CreatedByID = (int)row["CreatedByID"],
				Updated = (DateTime)row["Updated"],
				UpdatedByID = (int)row["UpdatedByID"],
				Revision = ParseRevision((byte[])row["Revision"]),
				Deleted = (bool)row["Deleted"]
			};
			return group;
		}
		private Group ParseGroup(DataSet dbResult)
		{
			if (dbResult.Tables.Count == 0 || dbResult.Tables[0].Rows.Count == 0)
				return null;

			var group = ParseGroup(dbResult.Tables[0].Rows[0]);

			if (dbResult.Tables.Count > 1)
				group.Members = dbResult.Tables[1].Rows.OfType<DataRow>().Select(m => ParseMember(m));

			return group;
		}

		private SqlCommand GetCreateSql(Group group)
		{
			if (group.Members != null && group.Members.Any())
			{
				var sql = @"SET XACT_ABORT ON;
						BEGIN TRANSACTION;

						INSERT INTO Groups (Name, Confirmed, Created, CreatedByID, Updated, UpdatedByID, Deleted)
						OUTPUT inserted.*
						VALUES(@Name, @Confirmed, @Created, @CreatedByID, @Updated, @UpdatedByID, @Deleted)

						DECLARE @groupID int = SCOPE_IDENTITY();
						
						INSERT INTO Members(GroupID, MemberID, AccountID, Name, Confirmed, Created, CreatedByID, Updated, UpdatedByID, Deleted)
						OUTPUT inserted.*
						SELECT @groupID, MemberID, AccountID, Name, Confirmed, Created, CreatedByID, Updated, UpdatedByID, Deleted FROM @NewMembers;
						
						COMMIT TRANSACTION;";

				var cmd = new SqlCommand(sql);
				cmd.Parameters.AddWithValue("@Name", group.Name);
				cmd.Parameters.AddWithValue("@Confirmed", group.Confirmed);
				cmd.Parameters.AddWithValue("@Created", group.Created);
				cmd.Parameters.AddWithValue("@CreatedByID", group.CreatedByID);
				cmd.Parameters.AddWithValue("@Updated", group.Updated);
				cmd.Parameters.AddWithValue("@UpdatedByID", group.UpdatedByID);
				cmd.Parameters.AddWithValue("@Deleted", group.Deleted);

				var members = GetMembersDbTable(group.Members);
				cmd.Parameters.Add(new SqlParameter("@NewMembers", SqlDbType.Structured) { Value = members, TypeName = "CreateMemberTableType" });

				return cmd;
			}
			else
			{
				var sql = @"
						INSERT INTO Groups (Name, Confirmed, Created, CreatedByID, Updated, UpdatedByID, Deleted)
						OUTPUT inserted.*
						VALUES(@Name, @Confirmed, @Created, @CreatedByID, @Updated, @UpdatedByID, @Deleted)";

				var cmd = new SqlCommand(sql);
				cmd.Parameters.AddWithValue("@Name", group.Name);
				cmd.Parameters.AddWithValue("@Confirmed", group.Confirmed);
				cmd.Parameters.AddWithValue("@Created", group.Created);
				cmd.Parameters.AddWithValue("@CreatedByID", group.CreatedByID);
				cmd.Parameters.AddWithValue("@Updated", group.Updated);
				cmd.Parameters.AddWithValue("@UpdatedByID", group.UpdatedByID);
				cmd.Parameters.AddWithValue("@Deleted", group.Deleted);

				return cmd;
			}
		}
		private SqlCommand GetGroupWithMembersSql(int groupID)
		{
			var cmd = new SqlCommand("SELECT * FROM Groups WHERE ID = @groupID; SELECT * FROM Members WHERE GroupID = @GroupID;");
			cmd.Parameters.AddWithValue("@GroupID", groupID);
			return cmd;
		}
		private SqlCommand GetUpdateSql(Group group)
		{
			var sql = @"UPDATE Groups SET
							Name = @Name,
							Confirmed = @Confirmed,
							Created = @Created,
							CreatedByID = @CreatedByID,
							Updated = @Updated,
							UpdatedByID = @UpdatedByID,
							Deleted = @Deleted
						OUTPUT inserted.*
						WHERE ID = @ID ";

			var cmd = new SqlCommand(sql);
			cmd.Parameters.AddWithValue("@ID", group.ID);
			cmd.Parameters.AddWithValue("@Name", group.Name);
			cmd.Parameters.AddWithValue("@Confirmed", group.Confirmed);
			cmd.Parameters.AddWithValue("@Created", group.Created);
			cmd.Parameters.AddWithValue("@CreatedByID", group.CreatedByID);
			cmd.Parameters.AddWithValue("@Updated", group.Updated);
			cmd.Parameters.AddWithValue("@UpdatedByID", group.UpdatedByID);
			cmd.Parameters.AddWithValue("@Deleted", group.Deleted);

			return cmd;
		}
		private SqlCommand GetGroupsByAccountSql(int accountID, long minRevision, int batchSize)
		{
			var sql = $"SELECT TOP {batchSize} g.* FROM Members m JOIN Groups g ON g.ID = m.GroupID WHERE m.AccountID = @AccountID AND g.Revision > @Revision ORDER BY g.Revision";
			var cmd = new SqlCommand(sql);
			cmd.Parameters.AddWithValue("@AccountID", accountID);
			cmd.Parameters.AddWithValue("@Revision", minRevision);
			return cmd;
		}
	}
}
