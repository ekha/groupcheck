﻿using System;
using System.Linq;
using System.Data.SqlClient;
using System.Data;
using System.Collections.Generic;
using GroupCheck.ServerEntities;

namespace GroupCheck.ServerStorage
{
	public partial class MSSQLStorage : IServerStorageMembers
	{
		Member IServerStorageMembers.Create(Member member)
		{
			var cmd = GetCreateSql(member);
			var row = GetDataRow(cmd);
			var created = ParseMember(row);
			return created;
		}
		Member IServerStorageMembers.Get(int groupID, int memberID)
		{
			var table = GetSqlTable<Member>();
			var cmd = new SqlCommand($"SELECT * FROM {table} WHERE GroupID = groupID AND MemberID = @memberID");
			cmd.Parameters.AddWithValue("@groupID", groupID);
			cmd.Parameters.AddWithValue("@memberID", memberID);

			var row = GetDataRow(cmd);
			var member = ParseMember(row);
			return member;
		}
		Member IServerStorageMembers.Update(Member member)
		{
			var cmd = GetUpdateSql(member);
			var row = GetDataRow(cmd);
			var updated = ParseMember(row);
			return updated;
		}
		long IServerStorageMembers.UpdateField(int groupID, int memberID, string fieldName, object value)
		{
			var table = GetSqlTable<Member>();
			var sql = $"UPDATE {table} SET {fieldName} = @Value OUTPUT inserted.Revision WHERE GroupID = @groupID AND MemberID = @memberID";
			var cmd = new SqlCommand(sql);
			cmd.Parameters.AddWithValue("@groupID", groupID);
			cmd.Parameters.AddWithValue("@memberID", memberID);
			cmd.Parameters.AddWithValue("@Value", value);

			var result = ExecuteScalar(cmd);
			var revision = ParseRevision((byte[])result);
			return revision;
		}
		void IServerStorageMembers.Delete(int groupID, int memberID)
		{
			var table = GetSqlTable<Member>();
			var sql = $"DELETE FROM {table} WHERE GroupID = @groupID AND MemberID = @memberID";
			var cmd = new SqlCommand(sql);
			cmd.Parameters.AddWithValue("@groupID", groupID);
			cmd.Parameters.AddWithValue("@memberID", memberID);

			Execute(cmd);
		}
		IEnumerable<Member> IServerStorageMembers.GetMembersByGroup(int groupID, long minRevision, int batchSize)
		{
			var cmd = GetMembersByGroupSql(groupID, minRevision, batchSize + 1);
			using (var table = GetTable(cmd))
			{
				foreach (DataRow row in table.Rows)
				{
					yield return ParseMember(row);
				}
			}
		}

		private Member ParseMember(DataRow row)
		{
			if (row == null) return null;

			var member = new Member()
			{
				GroupID = (int)row["GroupID"],
				MemberID = (int)row["MemberID"],
				AccountID = (row["AccountID"] == DBNull.Value) ? (int?)null : (int?)row["AccountID"],
				Name = (string)row["Name"],

				Confirmed = (bool)row["Confirmed"],
				Created = (DateTime)row["Created"],
				CreatedByID = (int)row["CreatedByID"],
				Updated = (DateTime)row["Updated"],
				UpdatedByID = (int)row["UpdatedByID"],
				Revision = ParseRevision((byte[])row["Revision"]),
				Deleted = (bool)row["Deleted"]
			};
			return member;
		}

		private SqlCommand GetCreateSql(Member member)
		{
			var sql = @"INSERT INTO Members (GroupID, MemberID, AccountID, Name, Confirmed, Created, CreatedByID, Updated, UpdatedByID, Deleted)
						OUTPUT inserted.*
						VALUES (@GroupID, @MemberID, @AccountID, @Name, @Confirmed, @Created, @CreatedByID, @Updated, @UpdatedByID, @Deleted);";

			var cmd = new SqlCommand(sql);
			cmd.Parameters.AddWithValue("@GroupID", member.GroupID);
			cmd.Parameters.AddWithValue("@MemberID", member.MemberID);
			cmd.Parameters.AddWithValue("@AccountID", (object)member.AccountID ?? DBNull.Value);
			cmd.Parameters.AddWithValue("@Name", member.Name);
			cmd.Parameters.AddWithValue("@Confirmed", member.Confirmed);
			cmd.Parameters.AddWithValue("@Created", member.Created);
			cmd.Parameters.AddWithValue("@CreatedByID", member.CreatedByID);
			cmd.Parameters.AddWithValue("@Updated", member.Updated);
			cmd.Parameters.AddWithValue("@UpdatedByID", member.UpdatedByID);
			cmd.Parameters.AddWithValue("@Deleted", member.Deleted);

			return cmd;

		}
		private SqlCommand GetUpdateSql(Member member)
		{
			var sql = @"UPDATE Members SET
							AccountID = @AccountID,
							Name = @Name,
							Confirmed = @Confirmed,
							Created = @Created,
							CreatedByID = @CreatedByID,
							Updated = @Updated,
							UpdatedByID = @UpdatedByID,
							Deleted = @Deleted
						OUTPUT inserted.*
						WHERE GroupID = @GroupID AND MemberID = @MemberID";

			var cmd = new SqlCommand(sql);
			cmd.Parameters.AddWithValue("@GroupID", member.GroupID);
			cmd.Parameters.AddWithValue("@MemberID", member.MemberID);
			cmd.Parameters.AddWithValue("@AccountID", (object)member.AccountID ?? DBNull.Value);
			cmd.Parameters.AddWithValue("@Name", member.Name);
			cmd.Parameters.AddWithValue("@Confirmed", member.Confirmed);
			cmd.Parameters.AddWithValue("@Created", member.Created);
			cmd.Parameters.AddWithValue("@CreatedByID", member.CreatedByID);
			cmd.Parameters.AddWithValue("@Updated", member.Updated);
			cmd.Parameters.AddWithValue("@UpdatedByID", member.UpdatedByID);
			cmd.Parameters.AddWithValue("@Deleted", member.Deleted);

			return cmd;
		}
		private SqlCommand GetMembersByGroupSql(int groupID, long minRevision, int batchSize)
		{
			var sql = $"SELECT TOP {batchSize} * FROM Members WHERE GroupID = @GroupID AND Revision > @Revision ORDER BY Revision";
			var cmd = new SqlCommand(sql);
			cmd.Parameters.AddWithValue("@GroupID", groupID);
			cmd.Parameters.AddWithValue("@Revision", minRevision);
			return cmd;
		}

		private DataTable GetMembersDbTable(IEnumerable<Member> members)
		{
			var table = new DataTable();
			table.Columns.Add("GroupID", typeof(int));
			table.Columns.Add("MemberID", typeof(int));
			table.Columns.Add("AccountID", typeof(int));
			table.Columns.Add("Name", typeof(string));
			table.Columns.Add("Confirmed", typeof(bool));
			table.Columns.Add("Created", typeof(DateTime));
			table.Columns.Add("CreatedByID", typeof(int));
			table.Columns.Add("Updated", typeof(DateTime));
			table.Columns.Add("UpdatedByID", typeof(int));
			table.Columns.Add("Deleted", typeof(bool));

			if (members != null && members.Any())
			{
				foreach (var member in members)
				{
					var row = table.NewRow();
					row["GroupID"] = member.GroupID;
					row["MemberID"] = member.MemberID;
					row["AccountID"] = (object)member.AccountID ?? DBNull.Value;
					row["Name"] = member.Name;
					row["Confirmed"] = member.Confirmed;
					row["Created"] = member.Created;
					row["CreatedByID"] = member.CreatedByID;
					row["Updated"] = member.Updated;
					row["UpdatedByID"] = member.UpdatedByID;
					row["Deleted"] = member.Deleted;

					table.Rows.Add(row);
				}
			}

			return table;
		}
	}
}
