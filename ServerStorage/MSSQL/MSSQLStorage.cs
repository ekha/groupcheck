﻿using System;
using System.Linq;
using System.Data.SqlClient;
using System.Data;
using System.Collections.Generic;

namespace GroupCheck.ServerStorage
{
	public partial class MSSQLStorage : IServerStorage, IServerStorageAccounts, IServerStorageGroups, IServerStorageMembers, IServerStorageChecks
	{
		private readonly string _connectionString;
		private readonly string _host;
		private readonly string _user;
		private readonly string _pass;
		private readonly string _db;
		public string DB { get { return _db; } }

		private static readonly Dictionary<Type, string> _tables = new Dictionary<Type, string>()
		{
			{ typeof(ServerEntities.Account), "Accounts" },
			{ typeof(ServerEntities.Group), "Groups" },
			{ typeof(ServerEntities.Member), "Members" },
			{ typeof(ServerEntities.Check), "Checks" }
		};

		public MSSQLStorage(string connectionString)
		{
			var pars = connectionString.Split(';').ToDictionary(p => p.Split('=')[0].Trim().ToLower(), p => p.Split('=')[1]);
			pars.TryGetValue("data source", out _host);
			pars.TryGetValue("user id", out _user);
			pars.TryGetValue("password", out _pass);
			pars.TryGetValue("initial catalog", out _db);
			_connectionString = connectionString;
		}
		public MSSQLStorage(string host, string user, string pass, string db)
		{
			_host = host;
			_user = user;
			_pass = pass;
			_db = db;
			_connectionString = $"data source={_host};initial catalog={_db};persist security info=True;user id={_user};password={_pass};";
		}

		public IServerStorageAccounts Accounts { get { return this; } }
		public IServerStorageGroups Groups { get { return this; } }
		public IServerStorageMembers Members { get { return this; } }
		public IServerStorageChecks Checks { get { return this; } }

		private SqlConnection GetConnection()
		{
			return new SqlConnection(_connectionString);
		}

		public int Execute(SqlCommand cmd)
		{
			using (SqlConnection conn = GetConnection())
			{
				cmd.Connection = conn;
				using (cmd)
				{
					conn.Open();
					var rowsAffected = cmd.ExecuteNonQuery();
					return rowsAffected;
				}
			}
		}
		public int Execute(string sql, SqlParameter[] values = null)
		{
			return Execute(CommandType.Text, sql, values);
		}
		public int Execute(CommandType cmdType, string sql, SqlParameter[] values = null)
		{
			var cmd = PrepareSqlCommand(cmdType, sql, values);
			return Execute(cmd);
		}

		public object ExecuteScalar(SqlCommand cmd)
		{
			using (SqlConnection conn = GetConnection())
			{
				cmd.Connection = conn;
				using (cmd)
				{
					conn.Open();
					var result = cmd.ExecuteScalar();
					return result;
				}
			}
		}
		public object ExecuteScalar(string sql, SqlParameter[] values = null)
		{
			return ExecuteScalar(CommandType.Text, sql, values);
		}
		public object ExecuteScalar(CommandType cmdType, string sql, SqlParameter[] values = null)
		{
			var cmd = PrepareSqlCommand(cmdType, sql, values);
			return ExecuteScalar(cmd);
		}

		public DataRow GetDataRow(SqlCommand cmd)
		{
			using (SqlConnection conn = GetConnection())
			{
				cmd.Connection = conn;
				using (cmd)
				{
					conn.Open();
					using (var adapter = new SqlDataAdapter(cmd))
					{
						var table = new DataTable();
						adapter.Fill(table);
						if (table.Rows.Count == 0) return null;
						return table.Rows[0];
					}
				}
			}
		}
		public DataRow GetDataRow(string sql, SqlParameter[] values = null)
		{
			return GetDataRow(CommandType.Text, sql, values);
		}
		public DataRow GetDataRow(CommandType cmdType, string sql, SqlParameter[] values = null)
		{
			var cmd = PrepareSqlCommand(cmdType, sql, values);
			return GetDataRow(cmd);
		}

		public DataTable GetTable(SqlCommand cmd)
		{
			using (SqlConnection conn = GetConnection())
			{
				cmd.Connection = conn;
				using (cmd)
				{
					conn.Open();
					using (var adapter = new SqlDataAdapter(cmd))
					{
						var table = new DataTable();
						adapter.Fill(table);
						return table;
					}
				}
			}
		}
		public DataTable GetTable(string sql, SqlParameter[] values = null)
		{
			return GetTable(CommandType.Text, sql, values);
		}
		public DataTable GetTable(CommandType cmdType, string sql, SqlParameter[] values = null)
		{
			var cmd = PrepareSqlCommand(cmdType, sql, values);
			return GetTable(cmd);
		}

		public DataSet GetDataSet(SqlCommand cmd)
		{
			using (SqlConnection conn = GetConnection())
			{
				cmd.Connection = conn;
				using (cmd)
				{
					conn.Open();
					using (var adapter = new SqlDataAdapter(cmd))
					{
						var ds = new DataSet();
						adapter.Fill(ds);
						return ds;
					}
				}
			}
		}
		public DataSet GetDataSet(string sql, SqlParameter[] values = null)
		{
			return GetDataSet(CommandType.Text, sql, values);
		}
		public DataSet GetDataSet(CommandType cmdType, string sql, SqlParameter[] values = null)
		{
			var cmd = PrepareSqlCommand(cmdType, sql, values);
			return GetDataSet(cmd);
		}

		private static SqlCommand PrepareSqlCommand(CommandType cmdType, string sql, SqlParameter[] values = null)
		{
			var cmd = new SqlCommand(sql);
			cmd.CommandType = cmdType;
			if (values != null)
				cmd.Parameters.AddRange(values);
			return cmd;
		}
		private static long ParseRevision(byte[] timestamp)
		{
			if (timestamp == null || timestamp.Length == 0) return 0;
			return BitConverter.ToInt64(timestamp.Reverse().ToArray(), 0);
		}
		private static SqlCommand GetReadSql<T>(int id) where T : class
		{
			var table = GetSqlTable<T>();
			var sql = $"SELECT * FROM {table} WHERE ID = @ID";
			var cmd = new SqlCommand(sql);
			cmd.Parameters.AddWithValue("@ID", id);
			return cmd;
		}
		private static SqlCommand GetReadSql<T>(int groupID, int id) where T : class
		{
			var table = GetSqlTable<T>();
			var sql = $"SELECT * FROM {table} WHERE GroupID = groupID ID = @ID";
			var cmd = new SqlCommand(sql);
			cmd.Parameters.AddWithValue("@ID", id);
			return cmd;
		}
		private static SqlCommand GetUpdateFieldSql<T>(int id, string fieldName, object value) where T : class
		{
			var table = GetSqlTable<T>();
			var sql = $"UPDATE {table} SET {fieldName} = @Value OUTPUT inserted.Revision WHERE ID = @ID";
			var cmd = new SqlCommand(sql);
			cmd.Parameters.AddWithValue("@ID", id);
			cmd.Parameters.AddWithValue("@Value", value);
			return cmd;
		}
		private static SqlCommand GetDeleteSql<T>(int id) where T : class
		{
			var table = GetSqlTable<T>();
			var sql = $"DELETE FROM {table} WHERE ID = @ID";
			var cmd = new SqlCommand(sql);
			cmd.Parameters.AddWithValue("@ID", id);
			return cmd;
		}
		private static string GetSqlTable<T>() where T : class
		{
			if (_tables.ContainsKey(typeof(T)))
				return _tables[typeof(T)];
			else
				throw new NotSupportedException();
		}

		static Dictionary<Type, List<System.Reflection.PropertyInfo>> properties = new Dictionary<Type, List<System.Reflection.PropertyInfo>>();
		private T ParseRow<T>(DataRow row) where T : class, new()
		{
			if (row == null) return (T)null;

			if (!properties.ContainsKey(typeof(T)))
			{
				lock (properties)
				{
					if (!properties.ContainsKey(typeof(T)))
					{
						var allProps = typeof(T).GetProperties();
						properties[typeof(T)] = allProps.ToList();
					}
				}
			}

			var props = properties[typeof(T)];

			var obj = new T();
			foreach (var p in props)
			{
				if (p.Name == "Revision")
					p.SetValue(obj, ParseRevision((byte[])row[p.Name]));
				else
					p.SetValue(obj, row[p.Name]);
			}
			return obj;
		}
	}
}
