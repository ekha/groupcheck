﻿using System;
using System.Linq;
using System.Data.SqlClient;
using System.Data;
using System.Collections.Generic;
using GroupCheck.ServerEntities;

namespace GroupCheck.ServerStorage
{
	public partial class MSSQLStorage : IServerStorageChecks
	{
		Check IServerStorageChecks.Create(Check check)
		{
			var cmd = GetCreateSql(check);
			var row = GetDataRow(cmd);
			var created = ParseCheck(row);
			return created;
		}
		Check IServerStorageChecks.Get(int groupID, int checkID)
		{
			var table = GetSqlTable<Check>();
			var cmd = new SqlCommand($"SELECT * FROM {table} WHERE GroupID = groupID AND CheckID = @checkID");
			cmd.Parameters.AddWithValue("@groupID", groupID);
			cmd.Parameters.AddWithValue("@checkID", checkID);

			var row = GetDataRow(cmd);
			var check = ParseCheck(row);
			return check;
		}
		Check IServerStorageChecks.Update(Check check)
		{
			var cmd = GetUpdateSql(check);
			var row = GetDataRow(cmd);
			var updated = ParseCheck(row);
			return updated;
		}
		long IServerStorageChecks.UpdateField(int groupID, int checkID, string fieldName, object value)
		{
			switch (fieldName)
			{
				case nameof(Check.GroupID):
				case nameof(Check.DateAndTime):
				case nameof(Check.Description):
				case nameof(Check.Confirmed):
				case nameof(Check.Created):
				case nameof(Check.CreatedByID):
				case nameof(Check.Updated):
				case nameof(Check.UpdatedByID):
				case nameof(Check.Deleted):
					break;

				case nameof(Check.Creditors):
				case nameof(Check.Debitors):
					value = SerializeMembers((IDictionary<int, int>)value);
					break;
				default:
					throw new ArgumentException(fieldName);
			}

			var table = GetSqlTable<Check>();
			var sql = $"UPDATE {table} SET {fieldName} = @Value OUTPUT inserted.Revision WHERE GroupID = @groupID AND CheckID = @checkID";
			var cmd = new SqlCommand(sql);
			cmd.Parameters.AddWithValue("@groupID", groupID);
			cmd.Parameters.AddWithValue("@checkID", checkID);
			cmd.Parameters.AddWithValue("@Value", value);

			var result = ExecuteScalar(cmd);
			var revision = ParseRevision((byte[])result);
			return revision;
		}
		void IServerStorageChecks.Delete(int groupID, int checkID)
		{
			var table = GetSqlTable<Check>();
			var sql = $"DELETE FROM {table} WHERE GroupID = @groupID AND CheckID = @checkID";
			var cmd = new SqlCommand(sql);
			cmd.Parameters.AddWithValue("@groupID", groupID);
			cmd.Parameters.AddWithValue("@checkID", checkID);

			Execute(cmd);
		}
		IEnumerable<Check> IServerStorageChecks.GetChecksByGroup(int groupID, long minRevision, int batchSize)
		{
			var cmd = GetChecksByGroupGroupSql(groupID, minRevision, batchSize + 1);
			using (var table = GetTable(cmd))
			{
				foreach (DataRow row in table.Rows)
				{
					yield return ParseCheck(row);
				}
			}
		}

		private Check ParseCheck(DataRow row)
		{
			if (row == null) return null;

			var check = new Check()
			{
				GroupID = (int)row["GroupID"],
				CheckID = (int)row["CheckID"],
				DateAndTime = (DateTime)row["DateAndTime"],
				Description = (string)row["Description"],
				Creditors = DeserializeMembers((string)row["Creditors"]),
				Debitors = DeserializeMembers((string)row["Debitors"]),

				Confirmed = (bool)row["Confirmed"],
				Created = (DateTime)row["Created"],
				CreatedByID = (int)row["CreatedByID"],
				Updated = (DateTime)row["Updated"],
				UpdatedByID = (int)row["UpdatedByID"],
				Revision = ParseRevision((byte[])row["Revision"]),
				Deleted = (bool)row["Deleted"]
			};
			return check;
		}

		private SqlCommand GetCreateSql(Check check)
		{
			var sql = @"INSERT INTO Checks (GroupID, CheckID, DateAndTime, Description, Creditors, Debitors, Confirmed, Created, CreatedByID, Updated, UpdatedByID, Deleted)
						OUTPUT inserted.*
						VALUES (@GroupID, @CheckID, @DateAndTime, @Description, @Creditors, @Debitors, @Confirmed, @Created, @CreatedByID, @Updated, @UpdatedByID, @Deleted);";

			var cmd = new SqlCommand(sql);
			cmd.Parameters.AddWithValue("@GroupID", check.GroupID);
			cmd.Parameters.AddWithValue("@CheckID", check.CheckID);
			cmd.Parameters.AddWithValue("@DateAndTime", check.DateAndTime);
			cmd.Parameters.AddWithValue("@Description", check.Description);
			cmd.Parameters.AddWithValue("@Creditors", SerializeMembers(check.Creditors));
			cmd.Parameters.AddWithValue("@Debitors", SerializeMembers(check.Debitors));
			cmd.Parameters.AddWithValue("@Confirmed", check.Confirmed);
			cmd.Parameters.AddWithValue("@Created", check.Created);
			cmd.Parameters.AddWithValue("@CreatedByID", check.CreatedByID);
			cmd.Parameters.AddWithValue("@Updated", check.Updated);
			cmd.Parameters.AddWithValue("@UpdatedByID", check.UpdatedByID);
			cmd.Parameters.AddWithValue("@Deleted", check.Deleted);

			return cmd;

		}
		private SqlCommand GetUpdateSql(Check check)
		{
			var sql = @"UPDATE Checks SET
							DateAndTime = @DateAndTime,
							Description = @Description,
							Confirmed = @Confirmed,
							Creditors = @Creditors,
							Debitors = @Debitors,
							Created = @Created,
							CreatedByID = @CreatedByID,
							Updated = @Updated,
							UpdatedByID = @UpdatedByID,
							Deleted = @Deleted
						OUTPUT inserted.*
						WHERE GroupID = @GroupID AND CheckID = @CheckID";

			var cmd = new SqlCommand(sql);
			cmd.Parameters.AddWithValue("@GroupID", check.GroupID);
			cmd.Parameters.AddWithValue("@CheckID", check.CheckID);
			cmd.Parameters.AddWithValue("@DateAndTime", check.DateAndTime);
			cmd.Parameters.AddWithValue("@Description", check.Description);
			cmd.Parameters.AddWithValue("@Creditors", SerializeMembers(check.Creditors));
			cmd.Parameters.AddWithValue("@Debitors", SerializeMembers(check.Debitors));
			cmd.Parameters.AddWithValue("@Confirmed", check.Confirmed);
			cmd.Parameters.AddWithValue("@Created", check.Created);
			cmd.Parameters.AddWithValue("@CreatedByID", check.CreatedByID);
			cmd.Parameters.AddWithValue("@Updated", check.Updated);
			cmd.Parameters.AddWithValue("@UpdatedByID", check.UpdatedByID);
			cmd.Parameters.AddWithValue("@Deleted", check.Deleted);

			return cmd;
		}
		private SqlCommand GetChecksByGroupGroupSql(int groupID, long minRevision, int batchSize)
		{
			var sql = $"SELECT TOP {batchSize} * FROM Checks WHERE GroupID = @GroupID AND Revision > @Revision ORDER BY Revision";
			var cmd = new SqlCommand(sql);
			cmd.Parameters.AddWithValue("@GroupID", groupID);
			cmd.Parameters.AddWithValue("@Revision", minRevision);
			return cmd;
		}

		private static string SerializeMembers(IDictionary<int, int> members)
		{
			if (members == null) return string.Empty;
			return string.Join(",", members.Select(m => $"{m.Key}:{m.Value}"));
		}
		private static IDictionary<int, int> DeserializeMembers(string members)
		{
			if (string.IsNullOrEmpty(members)) return new Dictionary<int, int>();
			return members.Split(',').ToDictionary(
				x => int.Parse(x.Split(':')[0]),
				x => int.Parse(x.Split(':')[1]));
		}
	}
}
