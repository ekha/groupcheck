﻿using System;
using System.Linq;
using System.Data.SqlClient;
using System.Data;
using GroupCheck.ServerEntities;

namespace GroupCheck.ServerStorage
{
	public partial class MSSQLStorage : IServerStorageAccounts
	{
		Account IServerStorageAccounts.Create(Account account)
		{
			var cmd = GetCreateSql(account);
			var row = GetDataRow(cmd);
			var created = ParseAccount(row);
			return created;
		}
		Account IServerStorageAccounts.Get(int accountID)
		{
			var cmd = GetReadSql<Account>(accountID);
			var row = GetDataRow(cmd);
			var account = ParseAccount(row);
			return account;
		}
		Account IServerStorageAccounts.Find(string email)
		{
			var cmd = GetAccountByEmailSql(email);
			var row = GetDataRow(cmd);
			var account = ParseAccount(row);
			return account;
		}
		Account IServerStorageAccounts.Update(Account account)
		{
			var cmd = GetUpdateSql(account);
			var row = GetDataRow(cmd);
			var updated = ParseAccount(row);
			return updated;
		}
		long IServerStorageAccounts.UpdateField(int accountID, string fieldName, object value)
		{
			var cmd = GetUpdateFieldSql<Account>(accountID, fieldName, value);
			var result = ExecuteScalar(cmd);
			var revision = ParseRevision((byte[])result);
			return revision;
		}
		void IServerStorageAccounts.Delete(int accountID)
		{
			var cmd = GetDeleteSql<Account>(accountID);
			Execute(cmd);
		}

		private Account ParseAccount(DataRow row)
		{
			if (row == null) return null;

			var account = new Account()
			{
				ID = (int)row["ID"],
				Name = (string)row["Name"],
				Email = (string)row["Email"],
				Salt = (string)row["Salt"],
				Password = (string)row["Password"],

				Created = (DateTime)row["Created"],
				Updated = (DateTime)row["Updated"],
				Revision = ParseRevision((byte[])row["Revision"]),
				Deleted = (bool)row["Deleted"]
			};
			return account;
		}

		private SqlCommand GetCreateSql(Account account)
		{
			var sql = @"INSERT INTO Accounts (Email, Name, Salt, Password, Created, Updated, Deleted)
						OUTPUT inserted.*
						VALUES (@Email, @Name, @Salt, @Password, @Created, @Updated, @Deleted);";

			var cmd = new SqlCommand(sql);
			cmd.Parameters.AddWithValue("@Email", account.Email);
			cmd.Parameters.AddWithValue("@Name", account.Name);
			cmd.Parameters.AddWithValue("@Salt", account.Salt);
			cmd.Parameters.AddWithValue("@Password", account.Password);
			cmd.Parameters.AddWithValue("@Created", account.Created);
			cmd.Parameters.AddWithValue("@Updated", account.Updated);
			cmd.Parameters.AddWithValue("@Deleted", account.Deleted);

			return cmd;
		}
		private SqlCommand GetAccountByEmailSql(string email)
		{
			var cmd = new SqlCommand("SELECT * FROM Accounts WHERE Email = @Email");
			cmd.Parameters.AddWithValue("@Email", email);
			return cmd;
		}
		private SqlCommand GetUpdateSql(Account account)
		{
			var sql = @"UPDATE Accounts SET
							Email = @Email,
							Name = @Name,
							Salt = @Salt,
							Password = @Password,
							Created = @Created,
							Updated = @Updated,
							Deleted = @Deleted
						OUTPUT inserted.*
						WHERE ID = @ID ";

			var cmd = new SqlCommand(sql);
			cmd.Parameters.AddWithValue("@ID", account.ID);
			cmd.Parameters.AddWithValue("@Email", account.Email);
			cmd.Parameters.AddWithValue("@Name", account.Name);
			cmd.Parameters.AddWithValue("@Salt", account.Salt);
			cmd.Parameters.AddWithValue("@Password", account.Password);
			cmd.Parameters.AddWithValue("@Created", account.Created);
			cmd.Parameters.AddWithValue("@Updated", account.Updated);
			cmd.Parameters.AddWithValue("@Deleted", account.Deleted);

			return cmd;
		}
	}
}
