CREATE TYPE [dbo].[CreateAccountTableType] AS TABLE(
	[Email] [nvarchar](100) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Salt] [nvarchar](50) NOT NULL,
	[Password] [nvarchar](50) NOT NULL,
	[Created] [datetime2](2) NOT NULL,
	[Updated] [datetime2](2) NOT NULL,
	[Deleted] [bit] NOT NULL
)
GO


CREATE TYPE [dbo].[CreateGroupTableType] AS TABLE(
	[Name] [nvarchar](50) NOT NULL,
	[Confirmed] [bit] NOT NULL,
	[Created] [datetime2](2) NOT NULL,
	[CreatedByID] [int] NOT NULL,
	[Updated] [datetime2](2) NOT NULL,
	[UpdatedByID] [int] NOT NULL,
	[Deleted] [bit] NOT NULL
)
GO


CREATE TYPE [dbo].[CreateMemberTableType] AS TABLE(
	[GroupID] [int] NOT NULL,
	[MemberID] [int] NOT NULL,
	[AccountID] [int] NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Confirmed] [bit] NOT NULL,
	[Created] [datetime2](2) NOT NULL,
	[CreatedByID] [int] NOT NULL,
	[Updated] [datetime2](2) NOT NULL,
	[UpdatedByID] [int] NOT NULL,
	[Deleted] [bit] NOT NULL
)
GO


CREATE TYPE [dbo].[CreateCheckTableType] AS TABLE(
	[GroupID] [int] NOT NULL,
	[CheckID] [int] NOT NULL,
	[DateAndTime] [smalldatetime] NOT NULL,
	[Description] [nvarchar](50) NOT NULL,
	[Creditors] [nvarchar](1000) NOT NULL,
	[Debitors] [nvarchar](1000) NOT NULL,
	[Confirmed] [bit] NOT NULL,
	[Created] [datetime2](2) NOT NULL,
	[CreatedByID] [int] NOT NULL,
	[Updated] [datetime2](2) NOT NULL,
	[UpdatedByID] [int] NOT NULL,
	[Deleted] [bit] NOT NULL
)
GO


CREATE TABLE [dbo].[Accounts](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Email] [nvarchar](100) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Salt] [varchar](50) NOT NULL,
	[Password] [varchar](50) NOT NULL,
	[Created] [datetime2](2) NOT NULL,
	[Updated] [datetime2](2) NOT NULL,
	[Revision] [timestamp] NOT NULL,
	[Deleted] [bit] NOT NULL DEFAULT 0,
 CONSTRAINT [PK_Accounts] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


CREATE TABLE [dbo].[Groups](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Confirmed] [bit] NOT NULL,
	[Created] [datetime2](2) NOT NULL,
	[CreatedByID] [int] NOT NULL,
	[Updated] [datetime2](2) NOT NULL,
	[UpdatedByID] [int] NOT NULL,
	[Revision] [timestamp] NOT NULL,
	[Deleted] [bit] NOT NULL DEFAULT 0,
 CONSTRAINT [PK_Groups] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


CREATE TABLE [dbo].[Members](
	[GroupID] [int] NOT NULL,
	[MemberID] [int] NOT NULL,
	[AccountID] [int] NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Confirmed] [bit] NOT NULL,
	[Created] [datetime2](2) NOT NULL,
	[CreatedByID] [int] NOT NULL,
	[Updated] [datetime2](2) NOT NULL,
	[UpdatedByID] [int] NOT NULL,
	[Revision] [timestamp] NOT NULL,
	[Deleted] [bit] NOT NULL DEFAULT 0,
 CONSTRAINT [PK_Members] PRIMARY KEY CLUSTERED 
(
	[GroupID] ASC, [MemberID]
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


CREATE TABLE [dbo].[Checks](
	[GroupID] [int] NOT NULL,
	[CheckID] [int] NOT NULL,
	[DateAndTime] [smalldatetime] NOT NULL,
	[Description] [nvarchar](50) NOT NULL,
	[Creditors] [nvarchar](1000) NOT NULL,
	[Debitors] [nvarchar](1000) NOT NULL,
	[Confirmed] [bit] NOT NULL,
	[Created] [datetime2](2) NOT NULL,
	[CreatedByID] [int] NOT NULL,
	[Updated] [datetime2](2) NOT NULL,
	[UpdatedByID] [int] NOT NULL,
	[Revision] [timestamp] NOT NULL,
	[Deleted] [bit] NOT NULL DEFAULT 0,
 CONSTRAINT [PK_Checks] PRIMARY KEY CLUSTERED 
(
	[GroupID] ASC, [CheckID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO