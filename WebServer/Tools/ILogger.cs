﻿using System;
using System.Linq;
using System.Data.SqlClient;
using System.Data;
using System.Collections.Generic;
using GroupCheck.Tools;


namespace GroupCheck.ServerStorage
{
	public interface ILogger
	{
		LogLevel LogLevel { get; set; }
		long RequestID { get; set; }
		void Debug(string msg);
		void Info(string msg);
		void Warning(string msg);
		void Error(string msg);
		void Fatal(string msg);
		void Log(LogLevel logLevel, string msg);
		void LogRequest(DateTime requestTime, string method, short responseCode, int duration, string url, string request, string response);
		long GetLastRequestNbr();
	}

	public enum LogLevel
	{
		Debug = 0,
		Info = 1,
		Warn = 2,
		Error = 3,
		Fatal = 4
	}
}
