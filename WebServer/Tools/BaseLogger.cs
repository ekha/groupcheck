﻿using System;
using System.Linq;
using System.Data.SqlClient;
using System.Data;
using System.Collections.Generic;
using GroupCheck.Tools;


namespace GroupCheck.ServerStorage
{
	public abstract class BaseLogger : ILogger
	{
		public LogLevel LogLevel { get; set; }
		public long RequestID { get; set; }

		public void Debug(string msg)
		{
			if (LogLevel <= LogLevel.Debug)
				Log(LogLevel.Debug, msg);
		}
		public void Info(string msg)
		{
			if (LogLevel <= LogLevel.Info)
				Log(LogLevel.Info, msg);
		}
		public void Warning(string msg)
		{
			if (LogLevel <= LogLevel.Warn)
				Log(LogLevel.Warn, msg);
		}
		public void Error(string msg)
		{
			if (LogLevel <= LogLevel.Error)
				Log(LogLevel.Error, msg);
		}
		public void Fatal(string msg)
		{
			if (LogLevel <= LogLevel.Fatal)
				Log(LogLevel.Fatal, msg);
		}

		public abstract void Log(LogLevel logLevel, string msg);

		public abstract void LogRequest(DateTime requestTime, string method, short responseCode, int duration, string url, string request, string response);

		public abstract long GetLastRequestNbr();
	}
}
