﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;
using System.Web.Http;
using System.Security.Principal;

namespace GroupCheck.WebServer
{
	public class AdminAttribute : System.Web.Mvc.AuthorizeAttribute
	{
		protected override bool AuthorizeCore(HttpContextBase actionContext)
		{
			return false;
		}
	}
}
