﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;
using System.Web.Http;
using System.Security.Principal;
using GroupCheck.Tools;

namespace GroupCheck.WebServer
{
	public class OAuthToken
	{
		public string random { get; set; }
		public int exp { get; set; }
		public int AccountId { get; set; }
		public string Name { get; set; }
		public string Email { get; set; }
		public string Encode()
		{
			return JWT.Encode(this, Application.OAuthTokenKey);
		}
	}
}
