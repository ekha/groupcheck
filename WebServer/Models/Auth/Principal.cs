﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;
using System.Web.Http;
using System.Security.Principal;

namespace GroupCheck.WebServer
{
	public class Principal : IPrincipal
	{
		public IIdentity Identity { get; }
		public bool IsInRole(string role)
		{
			return false;
		}
		public Principal(Identity data)
		{
			Identity = data;
		}
	}
}
