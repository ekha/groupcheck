﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;
using System.Web.Http;
using System.Security.Principal;

namespace GroupCheck.WebServer
{
	public class Identity : IIdentity
	{
		public int AccountId { get; }
		public string Name { get; }
		public string Email { get; }
		public string AuthenticationType { get; }
		public bool IsAuthenticated { get; }

		public Identity() { }
		public Identity(int accountId, string name, string email, string authenticationType, bool isAuthenticated)
		{
			AccountId = accountId;
			Name = name;
			Email = email;
			AuthenticationType = authenticationType;
			IsAuthenticated = isAuthenticated;
		}
	}
}
