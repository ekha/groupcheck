﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using GroupCheck.WebApi;
using GroupCheck.ServerStorage;
using GroupCheck.ServerEntities;
using GroupCheck.Tools;
using GroupCheck.Tools.Extensions;
using GroupCheck.Server;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;

namespace GroupCheck.WebServer
{
	[Authorize]
	public class BaseApiController : ApiController
	{
		protected Principal principal { get { return HttpContext.Current.User as Principal; } }
		protected Identity user { get { return HttpContext.Current.User?.Identity as Identity; } }
		protected ILogger logger = ((Application)HttpContext.Current.ApplicationInstance).Logger;

		private IServer _server;
		protected IServer server
		{
			get
			{
				if (_server == null)
				{
					_server = new DefaultServer((HttpContext.Current.User?.Identity as Identity).AccountId, Application.Storage);
				}
				return _server;
			}
		}

		protected HttpResponseMessage GetPlainResponse(string body)
		{
			var response = new HttpResponseMessage(HttpStatusCode.OK);
			response.Content = new StringContent(body);
			response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
			return response;
		}
	}
}
