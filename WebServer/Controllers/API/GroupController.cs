﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using GroupCheck.WebApi;
using GroupCheck.Tools;
using GroupCheck.Tools.Extensions;

namespace GroupCheck.WebServer
{
	public class GroupController : BaseApiController, IGroupAPI
	{
		[HttpPost]
		public Group Create(NewGroup request)
		{
			var newGroup = ParseNewGroup(request);
			var group = server.Groups.Create(newGroup);
			var response = GetGroupResponse(group);
			return response;
		}

		[HttpGet]
		public Group Confirm(int id)
		{
			var group = server.Groups.Confirm(id);
			var response = GetGroupResponse(group);
			return response;
		}

		[HttpPost]
		public Group Update(UpdateGroup request)
		{
			var updateGroup = new ServerEntities.UpdateGroup()
			{
				ID = request.ID,
				Name = request.Name
			};
			var group = server.Groups.Update(updateGroup);
			var response = GetGroupResponse(group);
			return response;
		}



		internal static ServerEntities.NewGroup ParseNewGroup(NewGroup group)
		{
			group.Members = (group.Members ?? new List<NewMember>()).Where(m => m != null);
			return new ServerEntities.NewGroup()
			{
				Name = group.Name,
				Members = group.Members.Select(m => MemberController.ParseNewMember(m))
			};
		}
		internal static Group GetGroupResponse(ServerEntities.Group group)
		{
			group.Members = (group.Members ?? new List<ServerEntities.Member>()).Where(m => m != null);
			return new Group()
			{
				ID = group.ID,
				Name = group.Name,
				Created = group.Created,
				CreatedByID = group.CreatedByID,
				Updated = group.Updated,
				UpdatedByID = group.UpdatedByID,
				Revision = group.Revision,
				Members = group.Members?.Select(m => MemberController.GetMemberResponse(m))
			};
		}
	}
}
