﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using GroupCheck.WebApi;
using GroupCheck.Tools;
using GroupCheck.Tools.Extensions;

namespace GroupCheck.WebServer
{
	public class MemberController : BaseApiController, IMemberAPI
	{
		[HttpPost]
		public Member Create(NewMember request)
		{
			var newMember = ParseNewMember(request);
			var member = server.Members.Create(newMember);
			var response = GetMemberResponse(member);
			return response;
		}

		[Route("api/Member/Confirm/{groupID}/{memberID}")]
		[HttpGet]
		public Member Confirm(int groupID, int memberID)
		{
			var member = server.Members.Confirm(groupID, memberID);
			var response = GetMemberResponse(member);
			return response;
		}

		[HttpPost]
		public Member Update(UpdateMember request)
		{
			var updateMember = new ServerEntities.UpdateMember()
			{
				MemberID = request.MemberID,
				AccountID = request.AccountID,
				Name = request.Name
			};
			var member = server.Members.Update(updateMember);
			var response = GetMemberResponse(member);
			return response;
		}



		internal static ServerEntities.NewMember ParseNewMember(NewMember member)
		{
			return new ServerEntities.NewMember()
			{
				GroupID = member.GroupID,
				Name = member.Name,
				AccountID = member.AccountID
			};
		}
		internal static Member GetMemberResponse(ServerEntities.Member member)
		{
			return new Member()
			{
				MemberID = member.MemberID,
				GroupID = member.GroupID,
				Name = member.Name,
				AccountID = member.AccountID,
				Created = member.Created,
				CreatedByID = member.CreatedByID,
				Updated = member.Updated,
				UpdatedByID = member.UpdatedByID,
				Revision = member.Revision,
			};
		}
	}
}
