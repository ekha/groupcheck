﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using GroupCheck.WebApi;
using GroupCheck.ServerStorage;
using GroupCheck.Tools;
using GroupCheck.Tools.Extensions;

namespace GroupCheck.WebServer
{
	public class CheckController : BaseApiController, ICheckAPI
	{
		[HttpPost]
		public Check Create(NewCheck request)
		{
			var newCheck = new ServerEntities.NewCheck()
			{
				GroupID = request.GroupID,
				Description = request.Description,
				DateAndTime = request.DateAndTime,
				Debitors = request.Debitors,
				Creditors = request.Creditors
			};
			var check = server.Checks.Create(newCheck);
			var response = GetCheckResponse(check);
			return response;
		}

		[Route("api/Check/Confirm/{groupID}/{checkID}")]
		[HttpGet]
		public Check Confirm(int groupID, int checkID)
		{
			var check = server.Checks.Confirm(groupID, checkID);
			var response = GetCheckResponse(check);
			return response;
		}

		[HttpPost]
		public Check Update(UpdateCheck request)
		{
			var updateCheck = new ServerEntities.UpdateCheck()
			{
				CheckID = request.CheckID,
				DateAndTime = request.DateAndTime,
				Description = request.Description,
				Debitors = request.Debitors,
				Creditors = request.Creditors
			};
			var check = server.Checks.Update(updateCheck);
			var response = GetCheckResponse(check);
			return response;
		}



		internal static Check GetCheckResponse(ServerEntities.Check check)
		{
			return new Check()
			{
				CheckID = check.CheckID,
				GroupID = check.GroupID,
				DateAndTime = check.DateAndTime,
				Description = check.Description,
				Created = check.Created,
				CreatedByID = check.CreatedByID,
				Updated = check.Updated,
				UpdatedByID = check.UpdatedByID,
				Revision = check.Revision,
			};
		}
	}
}
