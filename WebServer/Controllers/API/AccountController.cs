﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using GroupCheck.WebApi;
using GroupCheck.Tools;
using GroupCheck.Tools.Extensions;
using System.Net;
using System.Net.Http;

namespace GroupCheck.WebServer
{
	public class AccountController : BaseApiController, IAccountAPI
	{
		[HttpPost]
		[AllowAnonymous]
		public RegisterResponse Register(NewAccount request)
		{
			var newAccount = new ServerEntities.NewAccount()
			{
				Email = request.Email,
				Name = request.Name,
				Password = request.Password
			};
			var account = server.Accounts.Register(newAccount);
			var token = CreateToken(account);
			var response = new RegisterResponse()
			{
				AccountID = account.ID,
				Email = account.Email,
				Name = account.Name,
				Token = token.Encode(),
				Revision = account.Revision,
			};
			return response;
		}

		[HttpPost]
		[AllowAnonymous]
		public GetTokenResponse GetToken(GetTokenRequest request)
		{
			var account = server.Accounts.LoginByEmail(request.Email, request.Password);
			var token = CreateToken(account);
			var body = token.Encode();
			var response = new GetTokenResponse() { AccountID = account.ID, Token = body };
			return response;
		}

		[HttpGet]
		public GetAccountResponse GetAccount()
		{
			var account = server.Accounts.GetCurrent();
			var response = new GetAccountResponse()
			{
				AccountID = account.ID,
				Email = account.Email,
				Name = account.Name,
				Revision = account.Revision
			};
			return response;
		}

		[HttpPost]
		public void ChangePassword(ChangePasswordRequest request)
		{
			server.Accounts.ChangePassword(request.Password);
		}



		private OAuthToken CreateToken(ServerEntities.Account account)
		{
			var token = new OAuthToken()
			{
				random = Password.GeneratePassword(16),
				exp = DateTime.UtcNow.AddDays(1).ToUnixTime(),
				AccountId = account.ID,
				Name = account.Name,
				Email = account.Email
			};
			return token;
		}
	}
}
