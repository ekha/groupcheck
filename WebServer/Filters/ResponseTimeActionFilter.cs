﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using System.Web;
using System.Web.Http.Filters;
using System.Web.Http.Controllers;
using GroupCheck.WebApi;

namespace GroupCheck.WebServer
{
	public class ResponseTimeActionFilter : ActionFilterAttribute
	{
		private const string ResponseTimeKey = "ResponseTimeKey";
		public override void OnActionExecuting(HttpActionContext actionContext)
		{
			HttpContext.Current.Items[ResponseTimeKey] = Stopwatch.StartNew();
		}
		public override void OnActionExecuted(HttpActionExecutedContext actionContext)
		{
			var stopwatch = (Stopwatch)HttpContext.Current.Items[ResponseTimeKey];
			stopwatch.Stop();
			HttpContext.Current.Items["HandlerExecutionTime"] = stopwatch.ElapsedMilliseconds;
		}
	}
}