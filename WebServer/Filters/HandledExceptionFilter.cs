﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;
using System.Web.Http;
using System.Security.Principal;
using System.Net;
using System.Net.Http;
using System.Web.Http.Filters;
using GroupCheck.WebApi;
using GroupCheck.Server;
using GroupCheck.Tools.Extensions;

namespace GroupCheck.WebServer
{
	public class HandledExceptionFilter : ExceptionFilterAttribute
	{
		public override void OnException(HttpActionExecutedContext context)
		{
			var logger = ((Application)HttpContext.Current.ApplicationInstance).Logger;
			var exception = context?.Exception;
			logger.Info($"{exception?.Message}: {exception?.StackTrace}");

			if (exception is AuthenticationRequiredException)
			{
				context.Response = GetResponse(HttpStatusCode.Unauthorized, exception.Message);
			}
			else if (exception is AccessDeniedException)
			{
				context.Response = GetResponse(HttpStatusCode.Forbidden, exception.Message);
			}
			else if (exception is NotFoundException)
			{
				context.Response = GetResponse(HttpStatusCode.NotFound, exception.Message);
			}
			else if (exception is DeletedException)
			{
				context.Response = GetResponse(HttpStatusCode.Gone, exception.Message);
			}
			else if (exception is AlreadyExistsException)
			{
				context.Response = GetResponse(HttpStatusCode.Conflict, ResponseCode.ALREADY_EXISTS);
			}
			else if (exception is ValidationException)
			{
				logger.Warning($"{exception.Message}: {exception.StackTrace}");
				context.Response = GetResponse(
					(HttpStatusCode)ExtendedHttpStatusCode.UnprocessableEntity,
					GetValidationErrorCode(((ValidationException)exception).ValidationCode));
			}
			else
			{
				logger.Fatal($"{exception?.Message} TRACE: {exception?.StackTrace}");
				context.Response = GetResponse(HttpStatusCode.InternalServerError, exception?.ToString());
			}
		}

		private static readonly Dictionary<ValidationCode, string> validationErrors = new Dictionary<ValidationCode, string>()
		{
			{ ValidationCode.AccountEmptyEmail, ResponseCode.EMPTY_EMAIL },
			{ ValidationCode.AccountInvalidEmail, ResponseCode.INVALID_EMAIL },
			{ ValidationCode.AccountInvalidPassword, ResponseCode.INVALID_PASSWORD },

			{ ValidationCode.GroupEmptyName, ResponseCode.EMPTY_GROUP_NAME },
			{ ValidationCode.MemberEmptyName, ResponseCode.EMPTY_MEMBER_NAME },
		};

		private static string GetValidationErrorCode(ValidationCode serverValidationCode)
		{
			if (!validationErrors.ContainsKey(serverValidationCode))
				return ResponseCode.UNDEFINED_ERROR;

			return validationErrors[serverValidationCode];
		}

		private static HttpResponseMessage GetResponse(HttpStatusCode code, string body = null)
		{
			return new HttpResponseMessage(code) { Content = new StringContent(body) };
		}
	}
}
