﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;
using System.Web.Http;
using System.Security.Principal;
using System.Net;
using System.Net.Http;
using System.Web.Http.Filters;
using System.Web.Http.Controllers;
using GroupCheck.WebApi;
using GroupCheck.Server;
using GroupCheck.Tools.Extensions;

namespace GroupCheck.WebServer
{
	public class RequireInputDataFilter : System.Web.Http.Filters.ActionFilterAttribute
	{
		public override void OnActionExecuting(HttpActionContext actionContext)
		{
			if (actionContext.ActionArguments.Any(_ => _.Value == null))
			{
				actionContext.Response = new HttpResponseMessage(HttpStatusCode.BadRequest)
				{
					Content = new StringContent(ResponseCode.EMPTY_REQUEST)
				};
			}
		}
	}
}
