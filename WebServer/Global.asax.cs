﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;
using System.Web.Http;
using System.Configuration;
using System.Diagnostics;
using Newtonsoft.Json;
using GroupCheck.Tools;
using GroupCheck.Tools.Extensions;
using GroupCheck.Server;
using GroupCheck.ServerStorage;

namespace GroupCheck.WebServer
{
	public class Application : HttpApplication
	{
		public static readonly byte[] OAuthTokenKey = ConfigurationManager.AppSettings["oauthTokenKey"].GetUTF8Bytes();
		public static readonly IServerStorage Storage = new MSSQLStorage(ConfigurationManager.ConnectionStrings["db"].ConnectionString);
		private static long _lastRequestID;

		public readonly SqlLogger Logger = new SqlLogger(ConfigurationManager.ConnectionStrings["logger"].ConnectionString) { LogLevel = LogLevel.Debug };

		void Application_Start(object sender, EventArgs e)
		{
			AreaRegistration.RegisterAllAreas();
			GlobalConfiguration.Configure(WebApiConfig.Register);
			RouteConfig.RegisterRoutes(RouteTable.Routes);
			JsonConvert.DefaultSettings = () => new JsonSerializerSettings
			{
				DateFormatString = "o"
			};
			_lastRequestID = Logger.GetLastRequestNbr();
		}

		private Stopwatch _requestSW;
		protected void Application_BeginRequest(object sender, EventArgs e)
		{
			Logger.RequestID = System.Threading.Interlocked.Increment(ref _lastRequestID);
			HttpContext.Current.Response.Filter = new OutputFilterStream(HttpContext.Current.Response.Filter);
			_requestSW = new Stopwatch();
			_requestSW.Start();
		}
		protected void Application_AuthenticateRequest(object sender, EventArgs e)
		{
			HttpContext.Current.User = GetDefaultPrincipal();

			var token = Request.Headers["Authorization"] ?? Request.Cookies["Token"]?.Value;

			if (string.IsNullOrEmpty(token)) return;

			var principal = ParsePrincipalFromOAuthToken(token);
			if (principal != null)
				HttpContext.Current.User = principal;
		}
		protected void Application_AcquireRequestState(object sender, EventArgs e)
		{
			if (HttpContext.Current.User?.Identity?.IsAuthenticated == true) return;
			if (HttpContext.Current.Session == null) return;
			if (string.IsNullOrEmpty((string)Session["Authorization"])) return;

			var token = (string)Session["Authorization"];
			var principal = ParsePrincipalFromOAuthToken(token);
			if (principal != null)
				HttpContext.Current.User = principal;
		}
		protected void Application_LogRequest(object sender, EventArgs e)
		{
			var requestBody = (string)null;
			if (Request.HttpMethod == "POST" || Request.HttpMethod == "PUT")
			{
				try
				{
					Request.InputStream.Seek(0, System.IO.SeekOrigin.Begin);
					using (var reader = new System.IO.StreamReader(Request.InputStream))
					{
						requestBody = reader.ReadToEnd();
					}
				}
				catch (Exception ex)
				{
					requestBody = "ERROR READING: " + ex.Message;
				}
			}

			var responseBody = (string)null;
			try
			{
				responseBody = ((OutputFilterStream)(Response.Filter)).ReadStream();
			}
			catch (Exception ex)
			{
				responseBody = "ERROR READING: " + ex.Message;
			}

			_requestSW.Stop();
			var requestExecutionTime = (int)_requestSW.ElapsedMilliseconds;

			Logger.LogRequest(
				Request.RequestContext.HttpContext.Timestamp.ToUniversalTime(),
				Request.HttpMethod,
				(short)Response.StatusCode,
				requestExecutionTime,
				Request.RawUrl,
				requestBody,
				responseBody);
		}

		protected void Application_Error()
		{
			var context = HttpContext.Current;
			if (context != null)
			{
				Logger.Fatal($"Application_Error: {context.Error.Message} TRACE: {context.Error.StackTrace}");

				RequestContext requestContext = ((MvcHandler)context.CurrentHandler).RequestContext;
				if (requestContext.HttpContext.Request.IsAjaxRequest())
				{
					context.Response.Clear();
					string controllerName = requestContext.RouteData.GetRequiredString("controller");
					IControllerFactory factory = ControllerBuilder.Current.GetControllerFactory();
					IController controller = factory.CreateController(requestContext, controllerName);
					ControllerContext controllerContext = new ControllerContext(requestContext, (ControllerBase)controller);

					JsonResult jsonResult = new JsonResult
					{
						Data = new { success = false, serverError = "500" },
						JsonRequestBehavior = JsonRequestBehavior.AllowGet
					};
					jsonResult.ExecuteResult(controllerContext);
					context.Response.End();
				}
				else
				{
					context.Response.Redirect("~/Error");
				}
			}
		}

		private Principal ParsePrincipalFromOAuthToken(string tokenStr)
		{
			if (tokenStr.StartsWith("Bearer "))
				tokenStr = tokenStr.Remove(0, "Bearer ".Length).Trim();

			try
			{
				var tokenPayload = JWT.Decode(tokenStr, OAuthTokenKey, true);
				var token = tokenPayload.ParseJSON<OAuthToken>();
				Logger.Info($"Account {token.AccountId} {token.Name} <{token.Email }>, exp: {token.exp.DecodeUnixTime()}");

				if (token.exp.DecodeUnixTime() < DateTime.UtcNow)
				{
					Logger.Info("Token expired");
					return null;
				}

				var identity = new Identity(token.AccountId, token.Name, token.Email, "Token", true);
				var principal = new Principal(identity);
				return principal;
			}
			catch (Exception ex)
			{
				Logger.Error($"Error parsing OAuth token [{tokenStr}]: {ex.Message}");
				return null;
			}
		}
		private Principal GetDefaultPrincipal()
		{
			var identity = new Identity();
			var principal = new Principal(identity);
			return principal;
		}
	}
}