﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace GroupCheck.WebServer
{
	public static class WebApiConfig
	{
		public static void Register(HttpConfiguration config)
		{
			config.Filters.Add(new HandledExceptionFilter());
			config.Filters.Add(new RequireInputDataFilter());
			//config.Filters.Add(new ResponseTimeActionFilter());

			config.MapHttpAttributeRoutes();

			config.Routes.MapHttpRoute(
				name: "DefaultApi",
				routeTemplate: "api/{controller}/{action}/{id}",
				defaults: new { id = RouteParameter.Optional }
			);
		}
	}
}
