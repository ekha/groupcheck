﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace GroupCheck.Tools
{
	public static class Runtime
	{
		public static string GetMethodFullName()
		{
			var st = new System.Diagnostics.StackTrace();
			var sf = st.GetFrame(1);
			var method = sf.GetMethod();
			var result = method.DeclaringType.FullName + "." + method.Name;
			return result;
		}
	}
}
