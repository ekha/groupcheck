﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace GroupCheck.Tools
{
	public static class JSON
	{
		public static string Serialize(object input)
		{
			return Serialize(input, Formatting.None);
		}

		public static string Serialize(object input, Formatting formatting)
		{
			var result = JsonConvert.SerializeObject(input, formatting);
			return result;
		}

		public static T Deserialize<T>(string input)
		{
			if (input == null)
				throw new ArgumentNullException(nameof(input));

			var result = JsonConvert.DeserializeObject<T>(input);
			return result;
		}

		public static JObject ToJObject(string input)
		{
			if (input == null)
				throw new ArgumentNullException(nameof(input));

			var result = JObject.Parse(input);
			return result;
		}
	}
}
