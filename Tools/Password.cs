﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;

namespace GroupCheck.Tools
{
	public static class Password
	{
		public static string DefaultHashAlgorithm = Hash.Algorithm.RS256;
		private const string PASSWORD_CHARS = "123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz";

		public static string GeneratePassword(int lenght)
		{
			var chars = new char[lenght];
			var bytes = Hash.GenerateBytes(lenght);
			var mod = PASSWORD_CHARS.Length;
			for (var i = 0; i < lenght; i++)
			{
				chars[i] = PASSWORD_CHARS[bytes[i] % mod];
			}
			var password = new string(chars);
			return password;
		}

		public static string CalculateHash(string password)
		{
			var hash = Hash.Calculate(UTF8.GetBytes(password), DefaultHashAlgorithm);
			var result = Base64.ToBase64(hash);
			return result;
		}

		public static string CalculateHash(string password, string salt)
		{
			return CalculateHash(password + salt);
		}

		public static string CalculateHash(string password, string salt, string globalSalt)
		{
			return CalculateHash(password + salt + globalSalt);
		}
	}
}
