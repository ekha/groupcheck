﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;

namespace GroupCheck.Tools.Extensions
{
	public static class PasswordExtension
	{
		public static string CalculateHash(this string password)
		{
			return Password.CalculateHash(password);
		}
		public static string CalculateHash(this string password, string salt)
		{
			return Password.CalculateHash(password, salt);
		}
		public static string CalculateHash(this string password, string salt, string globalSalt)
		{
			return Password.CalculateHash(password, salt, globalSalt);
		}
	}
}
