﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace GroupCheck.Tools.Extensions
{
	public static class JSONExtension
	{
		public static string ToJSON(this object input, Formatting formatting = Formatting.None)
		{
			return JSON.Serialize(input, formatting);
		}
		public static T ParseJSON<T>(this string input)
		{
			return JSON.Deserialize<T>(input);
		}
		public static JObject ToJObject(this string input)
		{
			return JSON.ToJObject(input);
		}
	}
}
