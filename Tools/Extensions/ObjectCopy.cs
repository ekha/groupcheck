﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace GroupCheck.Tools.Extensions
{
	public static class ObjectCopyExtensions
	{
		public static T CreateCopy<T>(this T original)
		{
			if (original == null)
				return default(T);
			var tmp = JsonConvert.SerializeObject(original);
			var result = JsonConvert.DeserializeObject<T>(tmp);
			return result;
		}
	}
}
